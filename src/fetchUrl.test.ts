import nock from 'nock';
import {fetchUrl} from './fetchUrl';
import 'jest-extended';
import {HttpError} from './error/httpError';

test('fetchUrl 200', async () => {
    nock('https://example.com')
        .get('/foo', undefined, {
            reqheaders: {
                'User-Agent': 'Mozilla/5.0 AppleWebKit',
                'accept-encoding': 'gzip, deflate',
            }
        })
        .times(1)
        .reply(200, 'Hello World');

    let body = await fetchUrl('https://example.com/foo');
    expect(body).toBe('Hello World');
});


test('fetchUrl 500', async () => {
    nock('https://example.com')
        .get('/foo')
        .times(3)
        .reply(500, 'My Server Error');

    await expect(fetchUrl('https://example.com/foo')).rejects.toSatisfy(error => {
        expect(error).toBeInstanceOf(HttpError);
        expect(error.message).toBe('Failed to fetch \'https://example.com/foo\': Response code 500 (Internal Server Error)');
        expect(error.extra).toEqual({
            responseStatus: '500',
            responseBody: 'My Server Error',
        });
        return true
    });
});

test('fetchUrl with options', async () => {
    nock('https://example.com')
        .post('/foo', 'my-body', {
            reqheaders: {
                'User-Agent': 'Mozilla/5.0 AppleWebKit',
                'x-my-header': 'hello world',
            }
        })
        .times(1)
        .reply(200, 'Hello World');

    let body = await fetchUrl('https://example.com/foo', {
        method: 'post',
        body: 'my-body',
        headers: {
            'x-my-header': 'hello world',
        },
    });
    expect(body).toBe('Hello World');
});
