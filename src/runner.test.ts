import {Source, SourcePriority} from "./source/source";
import {EventData} from "./source/eventData";
import {ParseError} from "./error/parseError";
import {FetchRequest, Runner} from "./runner";
import {Level, MockLogger} from "./logger";
import {Region, RegionVariant} from "./denkmal_data/region";
import {Coordinate} from "tsgeo/Coordinate";
import {DenkmalApi} from "./denkmal_api/client";
import nock from "nock";
import {EventTime} from "./source/eventTime";
import {DenkmalRepo} from "./denkmal_data/repo";
import {Venue} from "./denkmal_data/venue";

describe('Runner', () => {
    let runner: Runner;
    let logger: MockLogger;

    beforeEach(() => {
        let sources = [
            new MockSourceValid(),
            new MockSourceError(),
            new MockSourceErrorGeneric(),
        ];
        let api = new DenkmalApi('http://example.com/graphql', 'my-api-token');
        let regions = [
            new Region('id-1', RegionVariant.basel, 'Europe/Zurich', new Coordinate(47.55457, 7.59032)),
        ];
        let venues: Venue[] = [];
        let repo = new DenkmalRepo(regions, venues);
        logger = new MockLogger();
        runner = new Runner(logger, sources, api, repo);
    });

    test('fetch() and upload()', async () => {
        let requestBodyExpected = {
            "operationName": null,
            "variables": {
                "events": [
                    {
                        "regionId": "id-1",
                        "sourceUrl": "http://example.com",
                        "sourceIdentifier": "basel:my-source-valid",
                        "sourcePriority": 30,
                        "links": [],
                        "genres": [],
                        "venueName": "My Venue",
                        "description": "My event 1",
                        "from": "2016-01-04T20:00:00+01:00",
                        "until": "2016-01-04T23:00:00+01:00",
                        "hasTime": true,
                    },
                    {
                        "regionId": "id-1",
                        "sourceUrl": "http://example.com",
                        "sourceIdentifier": "basel:my-source-valid",
                        "sourcePriority": 30,
                        "links": [],
                        "genres": [],
                        "venueName": "My Venue",
                        "description": "My event 2",
                        "from": "2016-01-04T12:00:00+01:00",
                        "hasTime": false,
                    }
                ]
            },
            "query": "mutation ($events: [ScraperEventInput]!) {\n  addScraperEvents(events: $events)\n}\n"
        };
        let requestBodyActual;
        let requestNock = nock('http://example.com')
            .post('/graphql', (body: object) => {
                requestBodyActual = body;
                return true;
            })
            .reply(200, '{"data":{"addScraperEvents":[true, true]}}');
        let fetchResults = await runner.fetch(3);
        await runner.upload(fetchResults);
        expect(requestBodyActual).toEqual(requestBodyExpected);

        expect(logger.entries(Level.info)).toEqual([
            {
                "level": "info",
                "message": "Fetched 3 sources, with 2 errors",
                "fields": {
                    "event": "scraper_result",
                    "scraper_result_source_count": 3,
                    "scraper_result_source_list": "basel:my-source-valid\nbasel:my-source-error\nbasel:my-source-error-generic",
                    "scraper_result_error_count": 2,
                    "scraper_result_error_list": "basel:my-source-error\nbasel:my-source-error-generic",
                    "scraper_result_success_count": 1,
                    "scraper_result_success_list": "basel:my-source-valid",
                    "scraper_result_success_without_events_count": 0,
                    "scraper_result_success_without_events_list": "",
                    "scraper_result_total_events": 2
                },
            }
        ]);
        expect(logger.entries(Level.warn)).toHaveLength(2);
        expect(logger.entries(Level.warn)[0]).toMatchObject({
            "level": "warn",
            "message": "Fetch error for source basel:my-source-error",
            "fields": {
                "event": "scraper_error",
                "error_foo": 12,
                "error_message": "My error",
                "error_name": "Error",
                "scraper_error_sourceIdentifier": "basel:my-source-error"
            }
        });
        expect(logger.entries(Level.warn)[1]).toMatchObject({
            "level": "warn",
            "message": "Fetch error for source basel:my-source-error-generic",
            "fields": {
                "event": "scraper_error",
                "error_message": "Unexpected generic error",
                "error_name": "Error",
                "scraper_error_sourceIdentifier": "basel:my-source-error-generic"
            }
        });
        expect(requestNock.isDone()).toEqual(true);
    });

    test('fetch() and upload() with API error', async () => {
        let requestNock = nock('http://example.com')
            .post('/graphql', /.*/)
            .reply(500, 'My server error');
        let fetchResults = await runner.fetch(3);
        await runner.upload(fetchResults);

        expect(logger.entries(Level.error)).toHaveLength(1);
        expect(logger.entries(Level.error)[0]).toMatchObject({
            "level": "error",
            "message": "Source 'basel:my-source-valid': Error while uploading events: Network error: Unexpected token M in JSON at position 0",
            "fields": {
                "error_http_body": "My server error",
                "error_http_status_code": 500,
                "error_message": "Network error: Unexpected token M in JSON at position 0",
                "error_name": "Error",
            }
        });
        expect(requestNock.isDone()).toEqual(true);
    });
});

class MockSourceValid implements Source {
    sourceName(): string {
        return "my-source-valid";
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new MockSourceValid()];
    }

    fetch(request: FetchRequest): Promise<EventData[]> {
        let events = [
            new EventData(
                "My Venue",
                "My event 1",
                new EventTime('Europe/Zurich',
                    {date: [2016, 1, 4], time: [20, 0]},
                    {date: [2016, 1, 4], time: [23, 0]}
                ),
                [],
                [],
                "http://example.com"
            ),
            new EventData(
                "My Venue",
                "My event 2",
                new EventTime('Europe/Zurich',
                    {date: [2016, 1, 4]},
                ),
                [],
                [],
                "http://example.com"
            ),
        ];
        return Promise.resolve(events);
    }
}

class MockSourceError implements Source {
    sourceName(): string {
        return "my-source-error";
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new MockSourceError()];
    }

    fetch(request: FetchRequest): Promise<EventData[]> {
        throw new ParseError("My error", {foo: 12});
    }
}

class MockSourceErrorGeneric implements Source {
    sourceName(): string {
        return "my-source-error-generic";
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new MockSourceErrorGeneric()];
    }

    fetch(request: FetchRequest): Promise<EventData[]> {
        throw new Error("Unexpected generic error");
    }
}

