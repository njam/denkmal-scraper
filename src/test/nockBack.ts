import path from "path";
import nock from 'nock';

import {gunzipSync} from "zlib";

export {nockBack}

async function nockBack(fixtureName: string, fn: () => void, record: boolean = false) {
    let testPath = (jasmine as any).testPath;
    nock.back.fixtures = path.resolve(path.join(testPath, '..', '__nocks__'));
    if (record) {
        if (process.env.NOCK_FORCE_LOCKDOWN) {
            throw new Error(`nockBack() is called with record=true, but is disallowed by env var "NOCK_FORCE_LOCKDOWN".`)
        }
        nock.back.setMode('record');
        jest.setTimeout(30 * 1000)
    } else {
        nock.back.setMode('lockdown');
    }
    const {nockDone} = await nock.back(`${fixtureName}.json`, {afterRecord: afterRecord as any});
    await fn();
    await nockDone();
}

type NockDefinition = {
    rawHeaders: string[],
    response: any,
}

// See https://github.com/nock/nock/issues/1212#issuecomment-419888993
function afterRecord(nockDefs: NockDefinition[]) {
    return nockDefs.map(def => {
        const headers = def.rawHeaders.reduce<{ [key: string]: string }>((acc, curr, i, arr) => {
            if (i % 2 === 0) {
                acc[arr[i].toLowerCase()] = arr[i + 1].toLowerCase();
            }
            return acc;
        }, {});
        if (Array.isArray(def.response)) {
            def.response = def.response.join('');
        }
        if (
            headers['content-encoding'] === 'gzip'
        ) {
            def.response = gunzipSync(Buffer.from(def.response, 'hex')).toString('utf-8');
            delete headers['content-encoding'];
            def.rawHeaders = [];
            for (let key in headers) {
                def.rawHeaders.push(key);
                def.rawHeaders.push(headers[key]);
            }
        }
        return def;
    });
}
