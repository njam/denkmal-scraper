import moment, {Moment} from "moment-timezone";
import {FetchRequest} from "runner";

export {testFetchRequest}

function testFetchRequest(date: Moment | string, daysToFetch?: number): FetchRequest {
    let dateMoment;
    if (typeof date === 'string') {
        dateMoment = moment.tz(date, 'Europe/Zurich');
    } else {
        dateMoment = date;
    }
    if (daysToFetch == null) {
        daysToFetch = 10;
    }
    return FetchRequest.fromDaysToFetch(dateMoment, daysToFetch)
}
