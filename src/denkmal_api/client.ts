import fetch from 'cross-fetch';
import {createHttpLink} from 'apollo-link-http';
import ApolloClient, {ApolloError} from 'apollo-client'
import {InMemoryCache} from 'apollo-cache-inmemory';
import gql from "graphql-tag";
import * as de from '@mojotech/json-type-validation';
import {ScraperEvent} from "denkmal_api/scraperEvent";
import {ApiError} from "error/apiError";
import {Venue} from "denkmal_data/venue";

export {DenkmalApi}

class DenkmalApi {
    private apollo: ApolloClient<{}>;

    constructor(url: string, authToken: string) {
        let link = createHttpLink({
            fetch: fetch,
            uri: url,
            headers: {
                'authentication': `Bearer ${authToken}`,
            },
        });
        let cache = new InMemoryCache();
        this.apollo = new ApolloClient({link, cache});
    }

    async getVenues(): Promise<Venue[]> {
        let query = gql`
            query {
                regions {
                    id
                    venues {
                        id
                        name
                        facebookPageId
                    }
                }
            }
        `;
        type resultType = { regions: { id: string, venues: { id: string, name: string, facebookPageId: string | null }[] }[] };
        let decoder: de.Decoder<resultType> = de.object({
            regions: de.array(
                de.object({
                    id: de.string(),
                    venues: de.array(
                        de.object({
                            id: de.string(),
                            name: de.string(),
                            facebookPageId: de.oneOf(de.string(), de.constant(null)),
                        }),
                    ),
                }),
            ),
        });
        let result = await convertApolloErrors(async () => {
            return await this.apollo.query({query});
        });
        let data = decoder.runWithException(result.data);

        let venues: Venue[] = [];
        for (let region of data.regions) {
            let regionId = region.id;
            for (let venueData of region.venues) {
                let facebookPageId = venueData.facebookPageId || undefined;
                let venue = new Venue(venueData.id, venueData.name, regionId, facebookPageId);
                venues.push(venue);
            }
        }
        return venues;
    }

    async addScraperEvents(events: ScraperEvent[]): Promise<boolean[]> {
        if (events.length === 0) {
            return [];
        }

        let mutation = gql`
            mutation ($events: [ScraperEventInput]!) {
                addScraperEvents(
                    events: $events
                )
            }
        `;
        type resultType = { addScraperEvents: boolean[] };
        let decoder: de.Decoder<resultType> = de.object({
            addScraperEvents: de.array(de.boolean()),
        });
        let result = await convertApolloErrors(async () => {
            return await this.apollo.mutate({
                mutation,
                variables: {events}
            });
        });
        let data = decoder.runWithException(result.data);
        return data.addScraperEvents;
    }
}

async function convertApolloErrors<R>(fn: () => R): Promise<R> {
    try {
        return await fn();
    } catch (originalError) {
        let e = ApiError.fromError(originalError);
        if (originalError instanceof ApolloError) {
            if (originalError.networkError) {
                let errNet: any = originalError.networkError;
                if (errNet.statusCode) {
                    e.setExtra('http_status_code', errNet.statusCode);
                }
                if (errNet.bodyText) {
                    e.setExtra('http_body', errNet.bodyText);
                }
                if (errNet.result && Array.isArray(errNet.result.errors)) {
                    let graphqlErrorMessages = errNet.result.errors.map((e: Error) => e.message);
                    e.setExtra('graphql_errors', graphqlErrorMessages.join('\n'));
                }
            }
        }
        throw e;
    }
}
