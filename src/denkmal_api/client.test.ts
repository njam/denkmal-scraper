import {DenkmalApi} from "./client";
import nock from 'nock';
import {ApiError} from "../error/apiError";
import "jest-extended";

describe('DenkmalApi', () => {

    test('getVenues', async () => {
        let api = new DenkmalApi('http://example.com/graphql', 'my-api-token');
        let mockReponse = {
            data: {
                regions: [
                    {
                        id: 'region-1',
                        venues: [
                            {
                                id: 'venue-1',
                                name: 'Venue 1',
                                facebookPageId: 'facebook-page-1',
                                __typename: 'Venue',
                            },
                            {
                                id: 'venue-2',
                                name: 'Venue 2',
                                facebookPageId: null,
                                __typename: 'Venue',
                            },
                        ],
                        __typename: 'Region',
                    },
                    {
                        id: 'region-2',
                        venues: [
                            {
                                id: 'venue-3',
                                name: 'Venue 3',
                                facebookPageId: 'facebook-page-3',
                                __typename: 'Venue',
                            },
                        ],
                        __typename: 'Region',
                    },
                ]
            }
        };
        let requestBodyExpected = {
            "operationName": null,
            "variables": {},
            "query": "{\n  regions {\n    id\n    venues {\n      id\n      name\n      facebookPageId\n      __typename\n    }\n    __typename\n  }\n}\n",
        };
        let requestBodyActual;
        nock('http://example.com')
            .post('/graphql', (body: object) => {
                requestBodyActual = body;
                return true;
            })
            .reply(200, JSON.stringify(mockReponse));
        let result = await api.getVenues();
        expect(requestBodyActual).toEqual(requestBodyExpected);

        expect(result).toEqual([
            {
                "facebookPageId": "facebook-page-1",
                "id": "venue-1",
                "name": "Venue 1",
                "regionId": "region-1"
            },
            {
                "id": "venue-2",
                "name": "Venue 2",
                "regionId": "region-1"
            },
            {
                "facebookPageId": "facebook-page-3",
                "id": "venue-3",
                "name": "Venue 3",
                "regionId": "region-2"
            }
        ]);
    });

    test('getVenues with error', async () => {
        let api = new DenkmalApi('http://example.com/graphql', 'my-api-token');
        nock('http://example.com')
            .post('/graphql', /.*/)
            .reply(500, '{}');

        await expect(api.getVenues()).rejects.toSatisfy(error => {
            expect(error).toBeInstanceOf(ApiError);
            expect(error.message).toBe('Network error: Response not successful: Received status code 500');
            expect(error.extra).toEqual({
                http_status_code: 500,
            });
            return true
        });
    });

    test('addScraperEvents', async () => {
        let api = new DenkmalApi('http://example.com/graphql', 'my-api-token');
        let requestBodyExpected = {
            "operationName": null,
            "variables": {
                "events": [
                    {
                        "regionId": "region-1",
                        "sourceUrl": "http://example.com",
                        "sourceIdentifier": "source-1",
                        "sourcePriority": 10,
                        "links": [
                            {
                                "label": "label-1",
                                "url": "http://source1.com"
                            }
                        ],
                        "genres": [
                            "tag-1",
                            "tag-2"
                        ],
                        "venueName": "venue-1",
                        "description": "description-1",
                        "from": "2018-12-30T20:00:00Z",
                        "until": "2018-12-30T23:00:00Z",
                        "hasTime": true
                    },
                    {
                        "regionId": "region-1",
                        "sourceUrl": "http://example.com",
                        "sourceIdentifier": "source-1",
                        "sourcePriority": 12,
                        "links": [],
                        "genres": [],
                        "venueName": "venue-2",
                        "description": "description-2",
                        "from": "2018-12-30T20:00:00Z",
                        "hasTime": false
                    }
                ]
            },
            "query": "mutation ($events: [ScraperEventInput]!) {\n  addScraperEvents(events: $events)\n}\n"
        };
        let requestBodyActual;
        nock('http://example.com', {
            reqheaders: {
                authentication: 'Bearer my-api-token',
            },
        })
            .post('/graphql', (body: object) => {
                requestBodyActual = body;
                return true;
            })
            .reply(200, '{"data":{"addScraperEvents":[true, true]}}');

        let scraperEvents = [
            {
                regionId: 'region-1',
                sourceUrl: 'http://example.com',
                sourceIdentifier: 'source-1',
                sourcePriority: 10,
                links: [
                    {label: 'label-1', url: 'http://source1.com'},
                ],
                genres: ['tag-1', 'tag-2'],
                venueName: 'venue-1',
                description: 'description-1',
                from: '2018-12-30T20:00:00Z',
                until: '2018-12-30T23:00:00Z',
                hasTime: true,
            },
            {
                regionId: 'region-1',
                sourceUrl: 'http://example.com',
                sourceIdentifier: 'source-1',
                sourcePriority: 12,
                links: [],
                genres: [],
                venueName: 'venue-2',
                description: 'description-2',
                from: '2018-12-30T20:00:00Z',
                hasTime: false,
            },
        ];
        let result = await api.addScraperEvents(scraperEvents);
        expect(requestBodyActual).toEqual(requestBodyExpected);
        expect(result).toEqual([true, true]);
    });

    test('addScraperEvents with empty list', async () => {
        let api = new DenkmalApi('http://example.com/graphql', 'my-api-token');
        let result = await api.addScraperEvents([]);
        expect(result).toEqual([]);
    });

    test('addScraperEvents with error', async () => {
        let api = new DenkmalApi('http://example.com/graphql', 'my-api-token');
        nock('http://example.com')
            .post('/graphql', /.*/)
            .reply(400, '{"errors":[{"message":"My Error 1"},{"message":"My Error 2"}]}');

        let scraperEvents = [
            {
                regionId: 'region-1',
                sourceUrl: 'http://example.com',
                sourceIdentifier: 'source-1',
                sourcePriority: 10,
                links: [],
                genres: [],
                venueName: 'venue-2',
                description: 'description-2',
                from: '2018-12-30T20:00:00Z',
                hasTime: true,
            },
        ];
        await expect(api.addScraperEvents(scraperEvents)).rejects.toSatisfy(error => {
            expect(error).toBeInstanceOf(ApiError);
            expect(error.message).toBe('Network error: Response not successful: Received status code 400');
            expect(error.extra).toEqual({
                http_status_code: 400,
                graphql_errors: 'My Error 1\nMy Error 2',
            });
            return true
        });
    });
});
