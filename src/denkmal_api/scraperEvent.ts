import {Region} from "denkmal_data/region";
import {EventData} from "source/eventData";
import {SourcePriority} from "source/source";

export {ScraperEvent, createScraperEvent}

type ScraperEvent = {
    regionId: string;
    sourceUrl: string;
    sourceIdentifier: string;
    sourcePriority: SourcePriority;
    links: ScraperEventLink[];
    genres: string[];
    venueName: string;
    description: string;
    from: string;
    until?: string;
    hasTime: boolean;
}

type ScraperEventLink = {
    label: string;
    url: string;
};

function createScraperEvent(
    region: Region,
    sourceIdentifier: string,
    sourcePriority: SourcePriority,
    eventData: EventData,
): ScraperEvent {
    return {
        regionId: region.id,
        sourceUrl: eventData.sourceUrl,
        sourceIdentifier: sourceIdentifier,
        sourcePriority,
        links: eventData.links,
        genres: eventData.genres,
        venueName: eventData.venueName,
        description: eventData.description,
        from: eventData.time.fromAsISO8601WithDefaultTime(),
        until: eventData.time.untilAsISO8601WithDefaultTime(),
        hasTime: eventData.time.hasTime(),
    }
}
