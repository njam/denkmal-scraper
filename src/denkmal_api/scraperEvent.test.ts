import {createScraperEvent} from "./scraperEvent";
import {Region, RegionVariant} from "../denkmal_data/region";
import {Coordinate} from "tsgeo/Coordinate";
import {EventData} from "../source/eventData";
import {EventTime} from "../source/eventTime";

describe('createScraperEvent', () => {
    test('From without time', () => {
        let region = new Region("region-1", RegionVariant.basel, "Europe/Zurich", new Coordinate(47.55814, 7.58769));
        let eventData = new EventData(
            "My Venue",
            "My event 1",
            new EventTime('Europe/Zurich', {date: [2016, 1, 4], time: [20, 0]}),
            [{label: 'foo', url: 'http://link-foo.com'}],
            ['tag-1', 'tag-2'],
            "http://example.com"
        );
        let scraperData = createScraperEvent(region, 'source-1', 22, eventData);

        expect(scraperData).toEqual({
            "regionId": "region-1",
            "venueName": "My Venue",
            "description": "My event 1",
            "from": "2016-01-04T20:00:00+01:00",
            "until": undefined,
            "sourceIdentifier": "source-1",
            "sourcePriority": 22,
            "sourceUrl": "http://example.com",
            "links": [{label: 'foo', url: 'http://link-foo.com'}],
            "genres": ['tag-1', 'tag-2'],
            "hasTime": true
        });
    });
});
