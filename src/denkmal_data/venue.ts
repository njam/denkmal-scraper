export {Venue}

class Venue {
    id: string;
    name: string;
    regionId: string;
    facebookPageId?: string;

    constructor(id: string, name: string, regionId: string, facebookPageId?: string) {
        this.id = id;
        this.name = name;
        this.regionId = regionId;
        this.facebookPageId = facebookPageId;
    }
}
