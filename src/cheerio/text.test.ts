import $ from "cheerio";
import './text';

describe('textVisual()', () => {
    test('removes newlines in text nodes', () => {
        let $html = $('<span>Hello\nWorld</span>');
        expect($html.textVisual()).toEqual('Hello World');
    });

    test('preserves breaks', () => {
        let $html = $('<span>Hello<br>World<br>There</span>');
        expect($html.textVisual()).toEqual('Hello\nWorld\nThere');
    });

    test('trims the result', () => {
        let $html = $('<span> Hello<br></span>');
        expect($html.textVisual()).toEqual('Hello');
    });

    test('removes duplicate whitespace', () => {
        let $html = $('<span>Hello   World \u00A0 There</span>');
        expect($html.textVisual()).toEqual('Hello World There');
    });

    test('works recursively', () => {
        let $html = $('<span> Hello<br>World<span>This is a\ntest</span></span>');
        expect($html.textVisual()).toEqual('Hello\nWorldThis is a test');
    });

    test('adds newlines for p-tags', () => {
        let $html = $('<span>Hello<p>World</p>There</span>');
        expect($html.textVisual()).toEqual('Hello\nWorld\nThere');
    });

    test('ignores hyperlinks', () => {
        let $html = $('<span>Hello <a href="https://example.com">World</a> There</span>');
        expect($html.textVisual()).toEqual('Hello World There');
    });

    test('can handle empty HTML', () => {
        let $html = $('');
        expect($html.textVisual()).toEqual('');
    });

    test('preserves multiple newlines', () => {
        let $html = $('<span>Hello<br><br>World<br>There</span>');
        expect($html.textVisual()).toEqual('Hello\n\nWorld\nThere');
    });

    test('preserves tags in headings', () => {
        let $html = $('<span><h1>Hello<br>World</h1>There</span>');
        expect($html.textVisual()).toEqual('Hello\nWorld\nThere');
    });

    test('does not escape markdown', () => {
        let $html = $('<span>Hello<br>-World</span>');
        expect($html.textVisual()).toEqual('Hello\n-World');
    })
});
