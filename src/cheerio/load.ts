import cheerio from "cheerio";

export {$load}

function $load(html: string): Cheerio {
    return cheerio.load(html).root();
}
