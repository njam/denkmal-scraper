import {Source} from "source/source";
import {Err, Ok, Result} from "@usefultools/monads";
import {BaseError} from "error/baseError";
import {Logger} from "logger";
import moment, {Moment} from "moment-timezone";
import {Region} from "denkmal_data/region";
import {DenkmalApi} from "denkmal_api/client";
import {createScraperEvent, ScraperEvent} from "denkmal_api/scraperEvent";
import {DenkmalRepo} from "denkmal_data/repo";

export {Runner, FetchRequest}

type FetchData = Result<ScraperEvent[], BaseError>;
type FetchResult = { sourceIdentifier: string, data: FetchData }

class Runner {
    private logger: Logger;
    private sources: Source[];
    private api: DenkmalApi;
    private repo: DenkmalRepo;


    constructor(logger: Logger, sources: Source[], api: DenkmalApi, repo: DenkmalRepo) {
        this.logger = logger;
        this.sources = sources;
        this.api = api;
        this.repo = repo;
    }

    async fetch(daysToFetch: number): Promise<FetchResult[]> {
        this.logger.debug(`Fetching ${this.sources.length} sources for ${daysToFetch} days...`);
        let fetchResults = await this.fetchSources(daysToFetch);
        this.logResults(fetchResults);
        return fetchResults;
    }

    async upload(fetchResults: FetchResult[]) {
        for (let fetchResult of fetchResults) {
            if (fetchResult.data.is_ok()) {
                let sourceIdentifier = fetchResult.sourceIdentifier;
                let events = fetchResult.data.unwrap();
                try {
                    this.logger.debug(`Source '${sourceIdentifier}': Uploading ${events.length} events...`);
                    let created = await this.sendEvents(events);
                    this.logger.debug(`Source '${sourceIdentifier}': Backend accepted ${created} events as valid.`);
                } catch (e) {
                    this.logger.error(`Source '${sourceIdentifier}': Error while uploading events: ${e.message}`, {
                        error: e,
                    });
                }
            }
        }
    }

    private logResults(results: FetchResult[]) {
        let errors = results.filter(r => r.data.is_err());
        let successes = results.filter(r => r.data.is_ok());
        let successes_without_events = successes.filter(r => r.data.unwrap().length === 0);
        let total_events = successes.reduce((acc, r) => acc + r.data.unwrap().length, 0);
        this.logger.info(`Fetched ${results.length} sources, with ${errors.length} errors`, {
            'event': 'scraper_result',
            'scraper_result_source_count': results.length,
            'scraper_result_source_list': results.map(r => r.sourceIdentifier).join('\n'),
            'scraper_result_error_count': errors.length,
            'scraper_result_error_list': errors.map(r => r.sourceIdentifier).join('\n'),
            'scraper_result_success_count': successes.length,
            'scraper_result_success_list': successes.map(r => r.sourceIdentifier).join('\n'),
            'scraper_result_success_without_events_count': successes_without_events.length,
            'scraper_result_success_without_events_list': successes_without_events.map(r => r.sourceIdentifier).join('\n'),
            'scraper_result_total_events': total_events,
        });

        for (let result of errors) {
            this.logger.warn(`Fetch error for source ${result.sourceIdentifier}`, {
                'event': 'scraper_error',
                'error': result.data.unwrap_err(),
                'scraper_error_sourceIdentifier': result.sourceIdentifier,
            });
        }
    }

    private async fetchSources(daysToFetch: number): Promise<FetchResult[]> {
        return await Promise.all(await this.sources.map(async source => {
            let region = this.repo.getRegionByVariant(source.regionVariant());
            let request = FetchRequest.fromDaysToFetch(moment.tz(region.timeZone), daysToFetch)
            return this.fetchSource(source, region, request);
        }));
    }

    private async fetchSource(source: Source, region: Region, request: FetchRequest): Promise<FetchResult> {
        let data: FetchData;
        let sourceIdentifier = `${source.regionVariant()}:${source.sourceName()}`;
        try {
            let events = await source.fetch(request);
            let scraperEvents = events.map(e =>
                createScraperEvent(region, sourceIdentifier, source.sourcePriority(), e)
            );
            data = Ok(scraperEvents);
        } catch (e) {
            if (!(e instanceof BaseError)) {
                e = BaseError.fromError(e);
            }
            data = Err(e)
        }
        data.match({
            ok: events => this.logger.debug(`Source '${sourceIdentifier}': fetched ${events.length} events.`),
            err: error => this.logger.debug(`Source '${sourceIdentifier}': error: '${error.message}'`),
        });
        return {sourceIdentifier, data};
    }

    private async sendEvents(events: ScraperEvent[]): Promise<number> {
        let created = await this.api.addScraperEvents(events);
        return created.filter(b => b).length;
    }
}

class FetchRequest {
    now: Moment;
    dates: Moment[];

    constructor(now: Moment, dates: Moment[]) {
        now = now.clone().set({hour: 0, minute: 0, second: 0, millisecond: 0});
        this.now = now;
        this.dates = dates;
    }

    static fromDaysToFetch(now: Moment, daysToFetch: number): FetchRequest {
        let dates = [];
        for (let i = 0; i < daysToFetch; i++) {
            dates.push(now.clone().add(i, 'days'));
        }
        return new FetchRequest(now, dates)
    }

    dateMin(): Moment {
        return moment.min(this.dates);
    }

    dateMax(): Moment {
        return moment.max(this.dates);
    }

    containsEventDate(eventDate: Moment): boolean {
        let eventDateMin = this.dateMin();
        let eventDateMax = this.dateMax().clone().add(1, 'days');
        return eventDate >= eventDateMin && eventDate <= eventDateMax;
    }
}
