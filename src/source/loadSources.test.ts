import {loadSources} from "./loadSources";
import {loadConfig} from "config";
import {Venue} from "denkmal_data/venue";
import {DenkmalRepo} from "denkmal_data/repo";

describe('loadSources', () => {
    // Loading the config to get the 'regions'
    process.env.ELASTICSEARCH_URL = 'http://my-elasticsearch:9200';
    process.env.DENKMAL_API_TOKEN = 'my-api-token';
    let config = loadConfig('config/config.toml');

    let regions = config.regions;
    let venues: Venue[] = [];
    let repo = new DenkmalRepo(regions, venues);

    test('loadSources', () => {
        let sources = loadSources(repo);

        expect(sources.length).toBeGreaterThanOrEqual(3);
        for (let source of sources) {
            expect(source).toHaveProperty('sourceName');
            expect(source).toHaveProperty('regionVariant');
            expect(source).toHaveProperty('fetch');
        }
    });

    test('loadSources with filter', () => {
        let sources = loadSources(repo, 'helsinki');

        expect(sources.length).toEqual(1);
        expect(sources[0].sourceName()).toEqual('helsinki');
    });
});
