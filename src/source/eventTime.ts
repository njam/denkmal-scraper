import moment, {Moment} from "moment-timezone";
import {DateParseError} from "../error/dateParseError";
import * as util from "util";

export {EventTime, DateAndTime, YMD, HM, dateTimeToMoment, momentToDateTime}

let defaultTime: HM = [12, 0];

class EventTime {
    timeZone: string;
    from: DateAndTime;
    until?: DateAndTime;

    constructor(timeZone: string, from: DateAndTime, until?: DateAndTime) {
        from = validateAndCoerceDateTime(from, timeZone);
        if (until) {
            until = validateAndCoerceDateTime(until, timeZone);
            if (dateTimeToMoment(timeZone, until) <= dateTimeToMoment(timeZone, from)) {
                throw new DateParseError(`Until-date must be after from-date: '${util.inspect(from)}' vs '${util.inspect(until)}'`);
            }
        }

        this.timeZone = timeZone;
        this.from = from;
        this.until = until;
    }

    fromAsISO8601WithDefaultTime(): string {
        let from: DateAndTime = JSON.parse(JSON.stringify(this.from));
        if (!from.time) {
            from.time = defaultTime;
        }
        return dateTimeToMoment(this.timeZone, from).format();
    }

    untilAsISO8601WithDefaultTime(): string | undefined {
        if (!this.until) {
            return undefined;
        }
        let until: DateAndTime = JSON.parse(JSON.stringify(this.until));
        if (!until.time) {
            until.time = defaultTime;
        }
        return dateTimeToMoment(this.timeZone, until).format();
    }

    hasTime(): boolean {
        if (!this.from.time) {
            return false
        }
        if (this.until && !this.until.time) {
            return false
        }
        return true
    }

    toStringDebug(): string {
        let text = dateTimeToString(this.timeZone, this.from);
        if (this.until) {
            text += ' - ' + dateTimeToString(this.timeZone, this.until)
        }
        text += ` (${this.timeZone})`;
        return text;
    }

    fromAsMoment(): Moment {
        return dateTimeToMoment(this.timeZone, this.from)
    }
}

type DateAndTime = {
    date: YMD;
    time?: HM;
}
type YMD = [number, number, number];
type HM = [number, number];


function dateTimeToMoment(timeZone: string, dateTime: DateAndTime): Moment {
    let obj: { [key: string]: number } = {
        year: dateTime.date[0],
        month: dateTime.date[1] - 1,
        date: dateTime.date[2],
    };
    if (dateTime.time) {
        obj['hour'] = dateTime.time[0];
        obj['minute'] = dateTime.time[1];
    }
    return moment.tz(obj, timeZone);
}

function momentToDateTime(mom: Moment): DateAndTime {
    let date: YMD = [mom.year(), mom.month() + 1, mom.date()];
    let time: HM = [mom.hour(), mom.minute()];
    return {date, time};
}

function validateAndCoerceDateTime(dateTime: DateAndTime, timeZone: string): DateAndTime {
    let moment = dateTimeToMoment(timeZone, dateTime);
    if (!moment.isValid()) {
        throw new DateParseError(`DateTime is invalid: '${util.inspect(dateTime)}'`)
    }
    dateTime.date = [moment.year(), moment.month() + 1, moment.date()];
    if (dateTime.time) {
        dateTime.time = [moment.hour(), moment.minute()];
    }
    return dateTime;
}

function dateTimeToString(timeZone: string, dateTime: DateAndTime): string {
    let dateStrs = [
        dateTime.date[0].toString(),
        dateTime.date[1].toString().padStart(2, '0'),
        dateTime.date[2].toString().padStart(2, '0'),
    ];
    let text = dateStrs.join('-');
    if (dateTime.time) {
        let timeStrs = [
            dateTime.time[0].toString().padStart(2, '0'),
            dateTime.time[1].toString().padStart(2, '0'),
        ];
        text += ' ' + timeStrs.join(':');
    }
    return text;
}
