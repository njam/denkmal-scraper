import {joinPhrases, matchForce, replaceForce, splitForce, stringIncludesAnyCase} from "./textHelper";

test('joinPhrases', () => {
    let phrases = [
        'hello world',
        '',
        'Hallo Welt...',
        'Die Mitte.',
        'San José',
        'Der Schluss',
    ];

    expect(joinPhrases(phrases))
        .toEqual('Hello world. Hallo Welt... Die Mitte. San José. Der Schluss');
});

describe('replaceForce', () => {
    test('search string', () => {
        let res = replaceForce('Hello World', 'World', 'Welt');
        expect(res).toEqual('Hello Welt');
    });

    test('search regexp', () => {
        let res = replaceForce('Hello World', /w.rld/i, 'Welt');
        expect(res).toEqual('Hello Welt');
    });

    test('search not found', () => {
        expect(() => {
            replaceForce('Hello World', 'Globus', 'Welt');
        }).toThrow(/Cannot find pattern 'Globus' for replacement./);
    });
});

describe('matchForce', () => {
    test('numeric capture', () => {
        let res = matchForce('Hello World', /(.)(o)/);
        expect(res.groups).toEqual({});
        expect(res[0]).toEqual('lo');
        expect(res[1]).toEqual('l');
        expect(res[2]).toEqual('o');
    });

    test('numeric capture global', () => {
        let res = matchForce('Hello World', /(.)(o)/g);
        expect(res.groups).toEqual({});
        expect(res[0]).toEqual('lo');
        expect(res[1]).toEqual('Wo');
    });

    test('named capture', () => {
        let res = matchForce('Hello World', /(?<first>.)(?<second>o)/);
        expect(res.groups).toEqual({first: 'l', second: 'o'});
        expect(res[0]).toEqual('lo');
        expect(res[1]).toEqual('l');
        expect(res[2]).toEqual('o');
    });

    test('named capture global', () => {
        let res = matchForce('Hello World', /(?<first>.)(?<second>o)/g);
        expect(res.groups).toEqual({});
        expect(res[0]).toEqual('lo');
        expect(res[1]).toEqual('Wo');
    });

    test('no match', () => {
        expect(() => {
            matchForce('Hello World', /Globus/);
        }).toThrow(/Cannot find pattern '\/Globus\/'./);
    });
});

describe('splitForce', () => {
    test('by string', () => {
        let res = splitForce('Hello World There', ' ');
        expect(res).toEqual(['Hello', 'World', 'There']);
    });

    test('by regexp', () => {
        let res = splitForce('Hello World There', /\s/);
        expect(res).toEqual(['Hello', 'World', 'There']);
    });

    test('no match', () => {
        expect(() => {
            splitForce('Hello World', /X/);
        }).toThrow(/Cannot split by pattern '\/X\/'./);
    });
});


describe('stringIncludesAnyCase', () => {
    test('partial match', () => {
        expect(stringIncludesAnyCase('Hello WÖRLD', 'wörld')).toEqual(true);
        expect(stringIncludesAnyCase('Hello WÖRLD', 'world')).toEqual(false);
    });

    test('full match', () => {
        expect(stringIncludesAnyCase('WÖRLD', 'wörld')).toEqual(true);
        expect(stringIncludesAnyCase('WÖRLD', 'world')).toEqual(false);
    });
});
