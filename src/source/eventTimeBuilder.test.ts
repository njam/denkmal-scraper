import moment from "moment-timezone";
import {EventTimeBuilder} from "./eventTimeBuilder";

describe('EventTimeBuilder', () => {
    let builder: EventTimeBuilder;
    beforeEach(() => {
        builder = new EventTimeBuilder(moment.tz('2019-07-01T21:00:00', 'Europe/Zurich'));
    });

    describe('when data is missing', () => {
        test('missing from-date', () => {
            expect(() => {
                builder.from.setTime([20, 0]);
                builder.build();
            }).toThrow(/From is missing 'date'./);
        });

        test('missing from-time', () => {
            builder.from.setDate([2019, 1, 2]);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 1, 2], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('missing until-date', () => {
            builder.from.setDate([2019, 1, 2]);
            builder.from.setTime([20, 0]);
            builder.until.setTime([23, 0]);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 1, 2], "time": [20, 0]},
                "until": {"date": [2019, 1, 2], "time": [23, 0]},
                "timeZone": "Europe/Zurich"
            });
        });

        test('missing until-date, and time is before from-time', () => {
            builder.from.setDate([2019, 1, 2]);
            builder.from.setTime([20, 0]);
            builder.until.setTime([2, 0]);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 1, 2], "time": [20, 0]},
                "until": {"date": [2019, 1, 3], "time": [2, 0]},
                "timeZone": "Europe/Zurich"
            });
        });

        test('missing until-time', () => {
            builder.from.setDate([2019, 1, 2]);
            builder.from.setTime([20, 0]);
            builder.until.setDate([2019, 1, 3]);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 1, 2], "time": [20, 0]},
                "until": {"date": [2019, 1, 3], "time": undefined},
                "timeZone": "Europe/Zurich"
            });
        });
    });

    describe('setDateByMoment()', () => {
        test('changes date', () => {
            let mom = moment.tz('2019-07-02T19:00:00', 'Europe/Zurich');
            builder.from.setDateByMoment(mom);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 2], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });
    });

    describe('setByISO8601()', () => {
        test('date and time', () => {
            builder.from.setByISO8601('2019-08-10 20:00');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": [20, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('ISO 8601', () => {
            builder.from.setByISO8601('2019-08-10T20:00:00+02:00');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": [20, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('ISO 8601 different timezone', () => {
            builder.from.setByISO8601('2019-08-10T20:00:00+03:00');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": [19, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('something invalid', () => {
            expect(() => {
                builder.from.setByISO8601('something');
                builder.build();
            }).toThrow(/Date is invalid: 'something'/);
        });
    });

    describe('setByUnixTimestamp()', () => {
        test('sets time in correct timezone', () => {
            builder.from.setByUnixTimestamp(1547755200);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 1, 17], "time": [21, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });
    });

    describe('setByMatch()', () => {
        beforeEach(() => {
            builder.from.setDateByMoment(moment.tz('2019-07-01T21:00:00', 'Europe/Zurich'));
        });

        test('hours + minutes', () => {
            builder.from.setByMatch('Um 20:30 Uhr', /(?<hour>\d{1,2}):(?<minute>\d{2}) uhr/i);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 30]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('hours', () => {
            builder.from.setByMatch('Um 20 Uhr', /(?<hour>\d{1,2}) uhr/i);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('hours + minutes, but only hours match', () => {
            builder.from.setByMatch('Um 20 Uhr', /(?<hour>\d{1,2})(:(?<minute>\d{2}))? uhr/i);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        describe('matching AM/PM', () => {

            test('hours + minutes with "ampm"', () => {
                builder.from.setByMatch('Um 8:30pm', /(?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})/i);
                expect(builder.build()).toEqual({
                    "from": {"date": [2019, 7, 1], "time": [20, 30]},
                    "timeZone": "Europe/Zurich",
                    "until": undefined
                });
            });

            test('12:00am', () => {
                builder.from.setByMatch('Um 12:00am', /(?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})/i);
                expect(builder.build()).toEqual({
                    "from": {"date": [2019, 7, 1], "time": [0, 0]},
                    "timeZone": "Europe/Zurich",
                    "until": undefined
                });
            });

            test('2:00am', () => {
                builder.from.setByMatch('Um 2:00AM', /(?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})/i);
                expect(builder.build()).toEqual({
                    "from": {"date": [2019, 7, 1], "time": [2, 0]},
                    "timeZone": "Europe/Zurich",
                    "until": undefined
                });
            });

            test('12:00pm', () => {
                builder.from.setByMatch('Um 12:00pm', /(?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})/i);
                expect(builder.build()).toEqual({
                    "from": {"date": [2019, 7, 1], "time": [12, 0]},
                    "timeZone": "Europe/Zurich",
                    "until": undefined
                });
            });

            test('2:00pm', () => {
                builder.from.setByMatch('Um 2:00PM', /(?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})/i);
                expect(builder.build()).toEqual({
                    "from": {"date": [2019, 7, 1], "time": [14, 0]},
                    "timeZone": "Europe/Zurich",
                    "until": undefined
                });
            });

            test('2:00XX', () => {
                expect(() => {
                    builder.from.setByMatch('Um 2:00XX', /(?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})/i);
                    builder.build();
                }).toThrow(/Invalid AM\/PM/);
            });

        });

        test('day + month', () => {
            builder.from.setByMatch('Am 5.8.', /(?<day>\d{1,2})\.(?<month>\d{1,2})\./);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 5]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('day + month as string', () => {
            builder.from.setByMatch('Am 5. August', /(?<day>\d{1,2})\. (?<month>\p{L}+)/u);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 5]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('day + month + year', () => {
            builder.from.setByMatch('Am 5.8.21', /(?<day>\d{1,2})\.(?<month>\d{1,2})\.(?<year>\d{1,4})/);
            expect(builder.build()).toEqual({
                "from": {"date": [2021, 8, 5]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('nothing', () => {
            expect(() => {
                builder.from.setByMatch('Etwas anderes', /(?<hour>\d{1,2}):(?<minute>\d{2}) uhr/i);
                builder.build();
            }).toThrow(/Failed to match text/);
        });
    });

    describe('setByMatchOptional()', () => {
        beforeEach(() => {
            builder.from.setDateByMoment(moment.tz('2019-07-01T21:00:00', 'Europe/Zurich'));
        });

        test('hours + minutes', () => {
            let result = builder.from.setByMatchOptional('Um 20:30 Uhr', /(?<hour>\d{1,2}):(?<minute>\d{2}) uhr/i);
            expect(result).toEqual(true);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 30]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('nothing', () => {
            let result = builder.from.setByMatchOptional('Etwas anderes', /(?<hour>\d{1,2}):(?<minute>\d{2}) uhr/i);
            expect(result).toEqual(false);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });
    });

    describe('setByMatchOneOf()', () => {
        beforeEach(() => {
            builder.from.setDateByMoment(moment.tz('2019-07-01T21:00:00', 'Europe/Zurich'));
        });

        test('hours + minutes', () => {
            builder.from.setByMatchOneOf('Um 20:30 Uhr', [
                /Etwas anderes/i,
                /(?<hour>\d{1,2}):(?<minute>\d{2}) uhr/i,
                /Etwas anderes2/i,
            ]);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 30]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('nothing', () => {
            expect(() => {
                builder.from.setByMatchOneOf('Um 20:30 Uhr', [
                    /Etwas anderes/i,
                    /Etwas anderes2/i,
                ]);
            }).toThrow(/Failed to match text/);
        });
    });

    describe('setTimeByParts()', () => {
        beforeEach(() => {
            builder.from.setDateByMoment(moment.tz('2019-07-01T21:00:00', 'Europe/Zurich'));
        });

        test('as numbers', () => {
            builder.from.setTimeByParts(20, 15);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 15]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('as strings', () => {
            builder.from.setTimeByParts('20', '15');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [20, 15]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('as PM', () => {
            builder.from.setTimeByParts('09', '15', 'PM');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 1], "time": [21, 15]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('midnight 24:00', () => {
            builder.from.setTimeByParts('24', '00');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 7, 2], "time": [0, 0]},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('invalid hour', () => {
            expect(() => {
                builder.from.setTimeByParts(25, 0);
                builder.build();
            }).toThrow(/DateTime is invalid: '{ date: \[ 2019, 7, 1 ], time: \[ 25, 0 ] }'/);
        });

        test('invalid minute', () => {
            expect(() => {
                builder.from.setTimeByParts(12, -2);
                builder.build();
            }).toThrow(/DateTime is invalid: '{ date: \[ 2019, 7, 1 ], time: \[ 12, -2 ] }'/);
        });

    });


    describe('setDateByParts()', () => {
        test('day as number', () => {
            builder.from.setDateByParts(10, 8);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('day as string', () => {
            builder.from.setDateByParts('10', 8);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('month as string', () => {
            builder.from.setDateByParts(10, 'August');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('month as string UPPERCASE', () => {
            builder.from.setDateByParts(10, 'AUGUST');
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('month as number', () => {
            builder.from.setDateByParts(10, 8);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('month invalid', () => {
            expect(() => {
                builder.from.setDateByParts(10, 14);
                builder.build();
            }).toThrow(/Invalid month/);
        });

        test('day invalid', () => {
            expect(() => {
                builder.from.setDateByParts(32, 8);
                builder.build();
            }).toThrow(/DateTime is invalid: '{ date: \[ 2019, 8, 32 ], time: undefined }'/);
        });

        test('year as number', () => {
            builder.from.setDateByParts(10, 8, 2020);
            expect(builder.build()).toEqual({
                "from": {"date": [2020, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('year as string', () => {
            builder.from.setDateByParts(10, 8, '2020');
            expect(builder.build()).toEqual({
                "from": {"date": [2020, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('year as 2-digit string', () => {
            builder.from.setDateByParts(10, 8, '20');
            expect(builder.build()).toEqual({
                "from": {"date": [2020, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('year too far in future', () => {
            expect(() => {
                builder.from.setDateByParts(10, 'August', '2025');
                builder.build();
            }).toThrow(/Invalid year/);
        });

        test('year too far in past', () => {
            expect(() => {
                builder.from.setDateByParts(10, 'August', '2017');
                builder.build();
            }).toThrow(/Invalid year/);
        });

        test('year guess', () => {
            builder.from.setDateByParts(10, 8);
            expect(builder.build()).toEqual({
                "from": {"date": [2019, 8, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('year guess next-year', () => {
            builder.from.setDateByParts(10, 2);
            expect(builder.build()).toEqual({
                "from": {"date": [2020, 2, 10], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });

        test('year guess leap-year', () => {
            builder.from.setDateByParts(29, 2);
            expect(builder.build()).toEqual({
                "from": {"date": [2020, 2, 29], "time": undefined},
                "timeZone": "Europe/Zurich",
                "until": undefined
            });
        });
    });

    describe('isInPast()', () => {
        test('returns TRUE for past dates', () => {
            builder.from.setDate([2019, 1, 1]);
            expect(builder.from.isInPast()).toEqual(true);
        });

        test('returns FALSE for future dates', () => {
            builder.from.setDate([2019, 12, 1]);
            expect(builder.from.isInPast()).toEqual(false);
        });

        test('returns FALSE for today', () => {
            builder.from.setDate([2019, 7, 1]);
            expect(builder.from.isInPast()).toEqual(false);
        });

        test('fails without date', () => {
            expect(() => {
                builder.from.isInPast()
            }).toThrow(/missing 'date'/);
        });
    });

    describe('isBefore()', () => {
        test('returns TRUE if before', () => {
            builder.from.setDate([2019, 12, 24]);
            expect(builder.from.isBefore(moment.tz('2019-12-24T00:01:00', 'Europe/Zurich'))).toEqual(true);
        });

        test('returns FALSE if after', () => {
            builder.from.setDate([2019, 12, 24]);
            expect(builder.from.isBefore(moment.tz('2019-12-23T23:59:00', 'Europe/Zurich'))).toEqual(false);
        });

        test('returns FALSE if same', () => {
            builder.from.setDate([2019, 12, 24]);
            expect(builder.from.isBefore(moment.tz('2019-12-24T00:00:00', 'Europe/Zurich'))).toEqual(false);
        });

        test('fails without date', () => {
            expect(() => {
                builder.from.isBefore(moment.tz('2019-12-24T21:00:00', 'Europe/Zurich'))
            }).toThrow(/missing 'date'/);
        });
    });

});


