import moment, {Moment} from "moment-timezone";
import {DateParseError} from "../error/dateParseError";
import {DateAndTime, dateTimeToMoment, EventTime, HM, momentToDateTime, YMD} from "./eventTime";
import * as util from "util";

export {EventTimeBuilder, YearMonth}


class DateTimeBuilder {
    protected now: Moment;
    protected timeZone: string;
    protected date?: YMD;
    protected time?: HM;

    constructor(now: Moment, timeZone: string) {
        this.now = now;
        this.timeZone = timeZone;
    }

    setDate(date: YMD) {
        this.date = date;
    }

    setDateByMoment(mom: Moment) {
        mom = mom.clone().tz(this.timeZone);
        checkMoment(mom);
        this.setDate([mom.year(), mom.month() + 1, mom.date()]);
    }

    setDateByParts(day: number | string, month: number | string, year?: number | string) {
        day = parseInt(day + '');
        month = parseMonth(month);

        let yearNow = parseInt(this.now.format('Y'));
        if (year !== undefined) {
            if ((year + '').length === 2) {
                year = yearNow.toString().substr(0, 2) + year;
            }
            year = parseInt(year + '');
        } else {
            year = yearNow;
            let dateMin = this.now.clone().subtract(4, 'month');
            let dateGuess = dateTimeToMoment(this.timeZone, {date: [year, month, day]});
            if (!dateGuess.isValid()) {
                // check if the guessed date would be valid next year (leap year)
                if (dateTimeToMoment(this.timeZone, {date: [year + 1, month, day]}).isValid()) {
                    year += 1;
                }
            } else if (dateGuess < dateMin) {
                // Date is more than 4 months in past -> set to next year
                year += 1;
            }

        }

        this.setDate([year, month, day]);
    }

    setTime(time: HM) {
        this.time = time;
    }

    setTimeByParts(hours: number | string, mins: number | string = 0, ampm?: string) {
        let h = +hours;
        let m = +mins;
        if (ampm) {
            ampm = ampm.toLowerCase();
            if (ampm === 'am') {
                if (h === 12) {
                    h = 0;
                }
            } else if (ampm === 'pm') {
                if (h < 12) {
                    h += 12;
                }
            } else {
                throw new DateParseError(`Invalid AM/PM: '${ampm}'.`);
            }
        }
        this.setTime([h, m]);
    }

    setByMoment(mom: Moment) {
        mom = mom.clone().tz(this.timeZone);
        checkMoment(mom);
        this.setDate([mom.year(), mom.month() + 1, mom.date()]);
        this.setTime([mom.hour(), mom.minute()]);
    }

    /**
     * Parse a full date string.
     * Examples:
     * - 2019-08-10 20:00
     * - 2019-08-10T20:00:00Z
     * - 2019-08-10T20:00:00Z+02:00
     */
    setByISO8601(text: string) {
        let mom = moment.tz(text, moment.ISO_8601, this.timeZone);
        checkMoment(mom);
        this.setByMoment(mom);
    }

    setByUnixTimestamp(timestamp: number) {
        let mom = moment.unix(timestamp);
        checkMoment(mom);
        this.setByMoment(mom);
    }

    /**
     * Match text with date and/or time against a regexp.
     * The following named capture groups are used:
     * - hour
     * - minute
     * - day
     * - month
     * - year
     */
    setByMatch(text: string, re: RegExp) {
        this.setByMatchInternal(text, [re], false);
    }

    /**
     * Returns TRUE if a match was found
     */
    setByMatchOptional(text: string, re: RegExp): boolean {
        return this.setByMatchInternal(text, [re], true);
    }

    setByMatchOneOf(text: string, res: RegExp[]) {
        this.setByMatchInternal(text, res, false);
    }

    private setByMatchInternal(text: string, patterns: RegExp[], ignoreNoMatch: boolean = false): boolean {
        let result = null;
        for (let pattern of patterns) {
            result = text.match(pattern);
            if (result !== null) {
                break;
            }
        }
        if (null === result) {
            if (ignoreNoMatch) {
                return false;
            } else {
                if (patterns.length === 1) {
                    let pattern = patterns.find(_ => true);
                    throw new DateParseError(`Failed to match text '${text}' against regex '${pattern}'`);
                } else {
                    throw new DateParseError(`Failed to match text '${text}' against any of ${patterns.length} regexes '${patterns.join(', ')}'`);
                }
            }
        }

        let groups: { [key: string]: string } = result.groups || {};

        if (groups.hour) {
            let h = groups['hour'];
            let m = '0';
            if (groups.minute) {
                m = groups['minute'];
            }
            let ampm = groups['ampm'];
            this.setTimeByParts(h, m, ampm);
        }

        if (groups.day && groups.month) {
            let d = groups['day'];
            let m = groups['month'];
            let y;
            if (groups.year) {
                y = groups['year'];
            }
            this.setDateByParts(d, m, y);
        }

        return true;
    }

    isInPast(): boolean {
        let now = dateTimeToMoment(this.timeZone, {date: momentToDateTime(this.now).date});
        return this.isBefore(now);
    }

    isBefore(moment: Moment): boolean {
        if (!this.date) {
            throw new DateParseError(`DateTimeBuilder is missing 'date'.`);
        }
        let date = dateTimeToMoment(this.timeZone, {date: this.date});
        return date < moment;
    }

    isAfter(moment: Moment): boolean {
        if (!this.date) {
            throw new DateParseError(`DateTimeBuilder is missing 'date'.`);
        }
        let date = dateTimeToMoment(this.timeZone, {date: this.date});
        return date > moment;
    }

    protected _build(): DateAndTime {
        if (!this.date) {
            throw new DateParseError(`DateTimeBuilder is missing 'date'.`);
        }
        let year = this.date[0];
        let yearNow = parseInt(this.now.format('Y'));
        if (year < (yearNow - 1)) {
            throw new DateParseError(`Invalid year: '${year}' is too far in the past.`);
        }
        if (year > (yearNow + 2)) {
            throw new DateParseError(`Invalid year: '${year}' is too far in the future.`);
        }
        return {date: this.date, time: this.time};
    }
}

class FromBuilder extends DateTimeBuilder {
    build(): DateAndTime {
        if (!this.date) {
            throw new DateParseError(`From is missing 'date'.`);
        }
        return this._build();
    }
}

class UntilBuilder extends DateTimeBuilder {
    build(from: DateAndTime): DateAndTime | undefined {
        if (!this.date) {
            if (this.time) {
                let fromMom = dateTimeToMoment(this.timeZone, from);
                let untilMom = dateTimeToMoment(this.timeZone, {date: from.date, time: this.time});
                if (untilMom < fromMom) {
                    untilMom.add(1, 'day');
                }
                return momentToDateTime(untilMom);
            } else {
                return undefined;
            }
        }
        return this._build();
    }
}

class EventTimeBuilder {
    from: FromBuilder;
    until: UntilBuilder;
    timeZone: string;

    constructor(now: Moment) {
        let timeZone = now.tz();
        if (!timeZone) {
            throw new DateParseError(`Moment 'now' has no time zone assigned.`);
        }
        this.timeZone = timeZone;
        this.from = new FromBuilder(now, timeZone);
        this.until = new UntilBuilder(now, timeZone);
    }

    build(): EventTime {
        let from = this.from.build();
        let until = this.until.build(from);
        return new EventTime(this.timeZone, from, until);
    }
}

class YearMonth {
    month: number;
    year: number;

    constructor(month: number | string, year: number | string) {
        this.month = parseMonth(month);
        this.year = parseInt(year + '');
    }

    incrMonth() {
        this.month += 1;
        if (this.month > 12) {
            this.month = 1;
            this.year += 1;
        }
    }
}

function checkMoment(mom: Moment) {
    if (!mom.isValid()) {
        throw new DateParseError(`Date is invalid: ${util.inspect(mom.creationData().input)}`);
    }
}

function parseMonth(month: number | string): number {
    if (parseInt(month + '') >= 1 && parseInt(month + '') <= 12) {
        month = parseInt(month + '');
    } else {
        month = month.toString().toLocaleLowerCase();
        if (month in months) {
            month = months[month];
        } else {
            throw new DateParseError(`Invalid month: '${month}'.`);
        }
    }
    return month;
}

const months: { [key: string]: number } = {
    'jan': 1,
    'januar': 1,
    'january': 1,
    'feb': 2,
    'februar': 2,
    'february': 2,
    'mar': 3,
    'mär': 3,
    'märz': 3,
    'maerz': 3,
    'march': 3,
    'mrz': 3,
    'apr': 4,
    'april': 4,
    'mai': 5,
    'may': 5,
    'jun': 6,
    'juni': 6,
    'june': 6,
    'jul': 7,
    'juli': 7,
    'july': 7,
    'aug': 8,
    'august': 8,
    'sep': 9,
    'sept': 9,
    'september': 9,
    'okt': 10,
    'oct': 10,
    'oktober': 10,
    'october': 10,
    'nov': 11,
    'november': 11,
    'dez': 12,
    'dec': 12,
    'dezember': 12,
    'december': 12,
};
