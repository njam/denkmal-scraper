export {EventGenresBuilder}

class EventGenresBuilder {
    genres: Set<string> = new Set();

    add(genres: string[] | string) {
        function makeArray(genres: string[] | string): string[] {
            let result = [];
            genres = Array.isArray(genres) ? genres : [genres];
            for (let tag of genres) {
                for (tag of tag.split(/(?:,|\/| & |\|)/)) {
                    result.push(tag);
                }
            }
            return result;
        }

        genres = makeArray(genres)
            .map(g => g.toLocaleLowerCase())
            .map(g => g.trim())
            .filter(g => g.length > 0);
        for (let genre of genres) {
            this.genres.add(genre);
        }
    }

    build(): string[] {
        return Array.from(this.genres);
    }
}
