import cheerio from "cheerio";
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {joinDot, matchForce} from "source/textHelper";
import {Moment} from "moment-timezone";


export class Ubwg implements Source {
    private _regionVariant: RegionVariant;
    private city: string;


    constructor(regionVariant: RegionVariant, city: string) {
        this._regionVariant = regionVariant;
        this.city = city;
    }

    sourceName(): string {
        return `ubwg-city:${this.city}`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.ListingWebsite;
    }

    regionVariant(): RegionVariant {
        return this._regionVariant;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [
            new Ubwg(RegionVariant.zurich, 'zuerich'),
            new Ubwg(RegionVariant.basel, 'basel'),
        ];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let eventsByDay = await Promise.all(request.dates.map(async date => {
            return await this._fetchDate(date, request)
        }));
        return eventsByDay.flat();
    }

    async _fetchDate(date: Moment, request: FetchRequest): Promise<EventData[]> {
        let dateString = date.format('YYYY-MM-DD');
        let url = `https://ubwg.ch/events/kategorie/${this.city}/${dateString}/`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let events = $('.type-tribe_events').toArray().map($);
        return events.map($event => {
            let eventUrl = $event.find('h2 a').attr('href');
            let event = new EventDataBuilder(request.now, eventUrl);

            event.venueName = $event.find('.ubwg-tribe-date-venue-delimiter + a').textVisual();

            let title = $event.find('h2').textVisual();
            let description = $event.find('.description').textVisual();
            event.description = joinDot([title, description]);

            let timeMatch = matchForce(
                $event.find('.venue-event-list-date').textVisual(),
                /^\p{L}{2,15}, (?<day>\d{1,2})\. (?<month>\p{L}{2,15}) @ (?<from_hour>\d{1,2}):(?<from_minute>\d{2}) - (?<until_hour>\d{1,2}):(?<until_minute>\d{2})/u
            );
            event.time.from.setDateByParts(timeMatch.groups['day'], timeMatch.groups['month']);
            event.time.from.setTimeByParts(timeMatch.groups['from_hour'], timeMatch.groups['from_minute']);
            event.time.until.setTimeByParts(timeMatch.groups['until_hour'], timeMatch.groups['until_minute']);

            return event.build();
        });
    }

}
