import {Ubwg} from "./ubwg";
import {nockBack} from "test/nockBack";
import {RegionVariant} from "denkmal_data/region";
import {testFetchRequest} from "test/testFetchRequest";


describe('Ubwg', () => {

    test('ubwg-2022-02-04', async function () {
        await nockBack('ubwg-2022-02-04', async () => {
            let source = new Ubwg(RegionVariant.zurich, 'zuerich');
            let events = await source.fetch(testFetchRequest('2022-02-04'));
            expect(events).toMatchSnapshot();
        });
    });

});
