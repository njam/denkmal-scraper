import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinColon, joinComma, matchOptional} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";


export class Parkplatz implements Source {

    sourceName(): string {
        return `parkplatz`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Parkplatz()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://www.park-platz.org`;

        // Fetch this and the next month
        let beginningOfMonth = request.now.clone().startOf('month');
        let agendaMonths = [
            beginningOfMonth,
            beginningOfMonth.clone().add(1, 'month'),
        ];

        return (await Promise.all(agendaMonths.map(async agendaMonth => {
            return await this.fetchAgendaPage(urlBase, agendaMonth.year(), agendaMonth.month(), request)
        }))).flat();
    }

    async fetchAgendaPage(urlBase: string, year: number, month: number, request: FetchRequest): Promise<EventData[]> {
        let html = await fetchUrl(`${urlBase}/agenda/${year}${String(month).padStart(2, '0')}`);
        let $ = cheerio.load(html);

        let $events = Array.from($('main > ul > li > .eventExcerpt')).map($);
        return $events.flatMap($event => {
            let eventUrl = $event.find('> a').attr('href');
            let event = new EventDataBuilder(request.now, eventUrl);
            event.venueName = 'Mehrspur';
            event.links.push({label: event.venueName, url: eventUrl});

            // Time
            event.time.from.setByMatch(
                $event.find('time').attr('datetime'),
                /^(?<year>\d{4})-(?<month>\d{1,2})-(?<day>\d{1,2}) (?<hour>\d{1,2}):(?<minute>\d{2}):00$/u,
            );

            // Title
            let title = $event.find('.text h2').textVisual();
            event.description = title;

            // Only consider music-related events
            if (!event.description.match(/(musik|konzert|Vinyl|bar|party|sound)/ui)) {
                return [];
            }

            return [event.build()];
        });
    }

}
