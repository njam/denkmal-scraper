import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";


export class Mehrspur implements Source {

    sourceName(): string {
        return `mehrspur`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Mehrspur()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://www.mehrspur.ch/veranstaltungen`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = Array.from($('.veranstaltungen')).map($);
        return $events.map($event => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Mehrspur';
            event.description = $event.find('h3').textVisual();
            event.time.from.setByMatchOneOf($event.find('.event-date').textVisual(), [
                /^\p{L}{2} (?<day>\d{1,2})\.(?<month>\p{L}+)\.?$/u,
                /^(?<day>\d{1,2})\.?(?<month>\p{L}+)\.? - (\d{1,2})\.?(\p{L}+)\.?$/u,
            ]);
            event.time.from.setByMatch(
                $event.find('.event-time').textVisual(),
                /^(?<hour>\d{1,2}):(?<minute>\d{2})$/u
            );
            let urlEvent = $event.find('.more-details a').attr('href');
            if (urlEvent.length > 0) {
                event.links.push({label: 'mehrspur', url: urlEvent});
            }
            let genres = $event.attr('class').split(/\s/)
                .filter(c => c.match(/^genre-/))
                .map(c => c.replace(/^genre-/, ''));
            event.genres.add(genres);

            return event.build();
        });
    }

}
