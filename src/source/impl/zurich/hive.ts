import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {ParseError} from "error/parseError";
import {joinColon, stringIncludesAnyCase} from "source/textHelper";

export class Hive implements Source {

    private urlBase: string

    constructor() {
        this.urlBase = `https://www.hiveclub.ch`;
    }

    sourceName(): string {
        return `hive`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Hive()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        // Get the calendar for this and the next month
        let date1 = request.dateMin();
        let date2 = date1.clone().add(1, 'month');
        let months = [
            {year: date1.year(), month: date1.month() + 1},
            {year: date2.year(), month: date2.month() + 1},
        ]

        // Load events for each calendar-month
        let eventsByMonth: EventData[][] = await Promise.all(months.map(async month => {
            return await this._fetchCalendarMonth(request, month)
        }));
        return eventsByMonth.flat();
    }

    async _fetchCalendarMonth(request: FetchRequest, month: YearAndMonth): Promise<EventData[]> {
        let url = `${this.urlBase}/views/ajax`;
        let body = `view_name=calendar&view_display_id=block_1&date=${month.year}-${month.month}`
        let responseText = await fetchUrl(url, {
            method: 'post',
            headers: {
                'content-type': 'application/x-www-form-urlencoded; charset=UTF-8',
            },
            body
        });

        // The json response contains multiple "commands", one is of type "insert" and contains the HTML we need
        let json: { command: string, data: string }[] = JSON.parse(responseText);
        let commandInsert = json.find(command => command['command'] == 'insert')
        if (commandInsert === undefined) {
            throw new ParseError(`Response is missing "insert" command`, {response: responseText})
        }

        let eventLinks = this._extractEventLinksFromCalendarMonth(commandInsert['data']);
        return await Promise.all(eventLinks.map(async eventLink => {
            return await this._fetchEvent(request, eventLink)
        }));
    }

    // Extract any event links from the given HTML
    _extractEventLinksFromCalendarMonth(html: string): string[] {
        let patternLink = /href="(\/events\/([^"]+))"/g;
        let urls = [];
        let matches;
        while ((matches = patternLink.exec(html)) != null) {
            let url = matches[1]
            if (!url.startsWith('http')) {
                url = this.urlBase + url;
            }
            urls.push(url)
        }
        // Remove dups
        urls = urls.filter((v, i, a) => a.indexOf(v) === i);
        return urls
    }

    async _fetchEvent(request: FetchRequest, url: string): Promise<EventData> {
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $event = $('.contentbox-standardpage');

        let event = new EventDataBuilder(request.now, url);
        event.venueName = 'Hive';
        event.description = $event.find('h3').textVisual();
        event.time.from.setByMatch(
            $event.find('.field-name-field-date').textVisual(),
            /^(\p{L}+), (?<day>\d{1,2})\. (?<month>\p{L}+) (?<year>\d+)$/u,
        );
        let $time = $event.find('.field-name-field-time');
        if ($time.length > 0) {
            let timeText = $event.find('.field-name-field-time').textVisual();
            if (['all day long', 'durchgehend bis mitternacht'].includes(timeText.toLocaleLowerCase())) {
                // Full-day event
            } else if (timeText.includes(' - ')) {
                // Multi-day event (e.g. Silvester)
                let [timeTextFrom, timeTextUntil] = timeText.split(' - ', 2);
                let patternDateTime = /^((?<day>\d{1,2})\.(?<month>\d{1,2})\.(?<year>\d{2}): )?(?<hour>\d{1,2})[:.](?<minute>\d{2})( Uhr)?$/u;
                event.time.from.setByMatch(timeTextFrom, patternDateTime);
                event.time.until.setByMatch(timeTextUntil, patternDateTime);
            } else {
                // Normal event
                event.time.from.setByMatch(timeText, /^(?<hour>\d{1,2})[:.](?<minute>\d{2})$/u);
            }
        } else {
            event.time.from.setTime([23, 0]);
        }
        event.links.push({label: 'hive', url});

        let title = $event.find('h2').textVisual();
        let description = title;

        let body = $event.find('.field-name-body').first().textVisual()
        let paragraphs = body.split(/\n(?:\s*\n\s*)+/);
        let paragraphDisco = paragraphs.find(p => /^Disco/i.test(p))
        if (paragraphDisco) {
            let lines = paragraphDisco.split(/\s*\n\s*/);
            let first_act = lines[1]
            if (first_act) {
                if (stringIncludesAnyCase(first_act, title)) {
                    description = first_act
                } else {
                    description = joinColon([title, first_act])
                }
            }
        }

        event.description = description;

        return event.build();
    }

}

type YearAndMonth = {
    year: number,
    month: number,
}
