import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";


export class Kontikibar implements Source {

    sourceName(): string {
        return `kontikibar`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Kontikibar()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://kontikibar.ch/`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = $('.cff-event').toArray().map($);
        return $events.map($event => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Kon-Tiki Bar';

            event.time.from.setByMatch($event.find('.cff-date > .cff-start-date').textVisual(),
                /^(?<month>\p{L}{2,10}) (?<day>\d{1,2}), (?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})$/ui
            );
            event.time.until.setByMatchOptional($event.find('.cff-date > .cff-end-date').textVisual(),
                /^- (?<month>\p{L}{2,10}) (?<day>\d{1,2}), (?<hour>\d{1,2}):(?<minute>\d{2})(?<ampm>\w{2})$/ui
            );

            let title = $event.find('.cff-event-title').textVisual();
            title = title
                .replace(/ at Kon-Tiki Bar$/u, '')
                .replace(/ at Kon-Tiki$/u, '')
                .replace(/ presented by Kon-Tiki$/u, '');
            event.description = title;

            let $links = $event.find('.cff-post-links > a').toArray().map($);
            let $linkFacebook = $links.find($l => $l.textVisual().includes('Facebook'));
            if ($linkFacebook) {
                event.links.push({label: 'facebook', url: $linkFacebook.attr('href')});
            }

            return event.build();
        });
    }

}
