import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl, getHtml} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {matchOptional} from "source/textHelper";

export class Zukunft implements Source {

    sourceName(): string {
        return `zukunft`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Zukunft()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://www.zukunft.cl`;
        let html = await fetchUrl(urlBase);
        let $ = cheerio.load(html);

        let eventUrls = $('#eventlist #eventlistcont .scroll > a').toArray().map($)
            .map($eventLink => $eventLink.attr('href'))
            .map(url => {
                if (!url.match(/^https?:\/\//)) {
                    url = urlBase + url;
                }
                return url;
            });

        type EventInfo = {
            url: string,
            date: string,
            info: string,
            title: string,
            lineup: string,
            starttimebar?: string,
            titlebar?: string,
            text: string,
        };
        let eventInfos: EventInfo[] = await Promise.all(eventUrls.map(async eventUrl => {
            let $html = await getHtml(eventUrl);
            return {
                url: eventUrl,
                date: $html.find('#event .ehead .edate').textVisual(),
                info: $html.find('#event .ehead .einfo').textVisual(),
                title: $html.find('#event .ehead .etitle').textVisual(),
                lineup: $html.find('#event .ehead .elineup').textVisual(),
                starttimebar: $html.find('#event .ehead .estarttimebar').textVisual() || undefined,
                titlebar: $html.find('#event .ehead .eltitlebar').textVisual() || undefined,
                text: $html.find('#event .hyphenate').textVisual(),
            };
        }));

        return eventInfos
            .filter(info => {
                return !info.text.includes('Weitere Informationen folgen');
            })
            .map(info => {
                let event = new EventDataBuilder(request.now, info.url);
                event.venueName = 'Zukunft';
                event.links.push({label: 'Zukunft', url: info.url});

                event.time.from.setByMatch(
                    info.date,
                    /^(\p{L}{2}) (?<day>\d{1,2}) (?<month>\d{1,2}) (?<year>\d{2})/u,
                );
                event.time.from.setByMatch(
                    info.info,
                    /^(?<hour>\d{1,2})(:(?<minute>\d{2}))?h\b/u,
                );

                let text = info.title + ' ' + info.lineup;
                if (info.titlebar) {
                    text += ', ';
                    let match = matchOptional(info.info, /Bar3000 offen ab (?<time>(\d{1,2})(:(\d{2}))?h)/);
                    if (match) {
                        text += `ab ${match.groups['time']} `;
                    }
                    text += `in der Bar3000: ${info.titlebar}`;
                }
                event.description = text;

                return event.build();
            });
    }

}
