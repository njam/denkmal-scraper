import {Parkplatz} from "./parkplatz";
import {testFetchRequest} from "test/testFetchRequest";
import {nockBack} from "test/nockBack";

describe('Parkplatz', () => {

    test('parkplatz-2022-11-17', async function () {
        await nockBack('parkplatz-2022-11-17', async () => {
            let source = new Parkplatz();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

});
