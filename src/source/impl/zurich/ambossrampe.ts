import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {ParseError} from "error/parseError";


export class Ambossrampe implements Source {

    sourceName(): string {
        return `ambossrampe`;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Ambossrampe()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://ambossrampe.ch/programm/`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = $('.mec-event-list-modern > .mec-event-article').toArray().map($);

        let events: EventData[] = [];
        for (let $event of $events) {
            let eventUrl = $event.find('.mec-event-title a').attr('href');
            let event = new EventDataBuilder(request.now, eventUrl);
            event.venueName = 'Amboss Rampe';
            event.links.push({label: event.venueName, url: eventUrl});

            // Place
            let place = $event.find('.mec-event-loc-place').textVisual();
            if (place != "Rampe") {
                continue;
            }

            // Title
            let title = $event.find('.mec-event-title').textVisual();
            let ignored = [
                'Geschlossen',
                'Geschlossene Gesellschaft',
            ];
            if (ignored.includes(title)) {
                continue;
            }
            event.description = title;

            // Date
            let dateDay = $event.find('.mec-event-date > .event-d').textVisual();
            let dateMonth = $event.find('.mec-event-date > .event-f').textVisual();
            event.time.from.setByMatch(
                `${dateDay} ${dateMonth}`,
                /^(?<day>\d{1,2}) (?<month>\p{L}+)$/u
            );
            // Don't consider events too far in the future, they can still be drafts missing basic details
            if (event.time.from.isAfter(request.dateMax())) {
                continue;
            }

            // Time
            let time = $event.find('.mec-start-time').textVisual();
            event.time.from.setByMatch(
                time,
                /^(?<hour>\d{2}):(?<minute>\d{2})$/u
            );

            events.push(event.build());
        }

        return events;
    }
}
