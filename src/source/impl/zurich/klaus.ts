import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {matchOptional} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";
import {ParseError} from "error/parseError";
import {BaseError} from "error/baseError";
import {YearMonth} from "source/eventTimeBuilder";


export class Klaus implements Source {

    sourceName(): string {
        return `klaus`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Klaus()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `http://hausvonklaus.ch/`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $content = $('.elementor-text-editor');
        let lines = $content.textVisual().split(/\n/);

        // Remove 'header' lines
        while (lines.length > 0) {
            let line = lines[0];
            if (
                line.trim() === '' ||
                line.match(/Dein Club von Freunden für Freunde/) ||
                line.match(/Die Partys von Klaus sind privat/) ||
                line.match(/Und vielleicht noch deren Freunde/) ||
                line.match(/Bist du ein Freund von Klaus/) ||
                line.match(/^Link$/)
            ) {
                lines.shift();
            } else {
                break;
            }
        }

        // Remove 'footer' lines
        let lineLast = lines.findIndex(l => !!l.match(/etwas bei Klaus verloren/));
        if (lineLast < 0) {
            throw new ParseError(`Cannot find beginning of 'footer' content.`);
        } else {
            lines = lines.slice(0, lineLast);
        }

        let paragraphs = lines
            .join('\n')
            .replace(/\r/g, '')
            .replace(/\n\s+\n/g, '\n\n')
            .split(/\n{2,}/gm)
            .map(p => p.trim())
            .filter((p) => p.length > 0);

        let events = [];
        let month: YearMonth | null = null;
        let dayPrevious;

        for (let paragraph of paragraphs) {
            let matchMonth = matchOptional(paragraph, /^(?<month>\p{L}+) (?<year>\d{4})$/mu);
            let matchEventLong = matchOptional(paragraph, /^(?<weekday>\p{L}+), (?<day>\d{1,2}). (?<month>\p{L}+) (?<year>\d{4})\n(?<desc>.+)$/um);
            let matchEventShort = matchOptional(paragraph, /^(?<day>\d{1,2})\.?( – (?<day2>\d{1,2}))? (?<desc>.+)$/um);
            if (matchMonth) {
                month = new YearMonth(matchMonth.groups['month'], matchMonth.groups['year']);
            } else if (paragraph.match(/Mitternacht bis spät/)) {
                continue;
            } else if (matchEventLong || matchEventShort) {
                let event = new EventDataBuilder(request.now, url);
                event.time.from.setTimeByParts(24, 0);
                event.venueName = 'Klaus';
                if (matchEventLong) {
                    event.time.from.setDateByParts(matchEventLong.groups['day'], matchEventLong.groups['month'], matchEventLong.groups['year']);
                    event.description = matchEventLong.groups['desc'];
                }
                if (matchEventShort) {
                    if (!month) {
                        throw new BaseError(`Matched event, without a previous month line.`, {paragraph, html})
                    }
                    let day = parseInt(matchEventShort.groups['day']);
                    if (dayPrevious && dayPrevious > day) {
                        // Event is from *next* month
                        month.incrMonth();
                    }
                    dayPrevious = day;
                    event.time.from.setDateByParts(day, month.month, month.year);
                    event.description = matchEventShort.groups['desc'];
                }
                event.time.from.setByMatchOptional(event.description || '', /\bab (?<hour>\d{1,2}) uhr\b/i);
                events.push(event.build());
            } else {
                throw new ParseError(`Unexpected paragraph: '${paragraph}'`, {paragraph, html});
            }
        }

        return events;
    }

}
