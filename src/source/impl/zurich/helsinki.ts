import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinColon, joinComma, joinDot} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";


export class Helsinki implements Source {

    sourceName(): string {
        return `helsinki`;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Helsinki()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://www.helsinkiklub.ch/`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = $('#program .event').toArray().map($);
        return $events.map($event => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Helsinki';

            let dateDay = $event.find('.date .day').textVisual();
            let dateMonth = $event.find('.date .month').textVisual();
            event.time.from.setDateByParts(dateDay, dateMonth);

            let timeText = $event.find('.agenda .showtime').textVisual();
            event.time.from.setByMatchOptional(timeText, /(?<hour>\d{1,2})(:(?<minute>\d{2}))?\suhr/i);
            event.time.from.setByMatchOptional(timeText, /show\s(?<hour>\d{1,2})(:(?<minute>\d{2}))?\suhr/i);

            function extractTextWithAddition($el: Cheerio) {
                let textAddition = $el.find('.addition').textVisual();
                let textDirect = $el.find('.addition').remove().end().textVisual();
                return joinColon([textDirect, textAddition]);
            }

            let descs = [];
            if ($event.has('.agenda .top')) {
                descs.push(extractTextWithAddition($event.find('.agenda .top')));
            }
            if ($event.has('.agenda .support')) {
                let support = extractTextWithAddition($event.find('.agenda .support'));
                support = support.replace(/\+ /, '');
                descs.push(support);
            }
            event.description = joinComma(descs);

            if ($event.has('.agenda .headline')) {
                let headline = $event.find('.agenda .headline').textVisual();
                if (headline.match(/geschlossen/)) {
                    event.description = joinDot([headline, event.description]);
                }
            }

            return event.build();
        });
    }

}
