import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinColon, joinComma, matchForce} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";


export class Friedasbuexe implements Source {

    sourceName(): string {
        return `friedasbuexe`;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Friedasbuexe()];
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://friedasbuexe.ch`;
        let url = `${urlBase}/programm/`;
        let html = await fetchUrl(url);
        let $ = cheerio.load(html);
        let $events = $('.jupiterx-main .jet-listing .jet-listing-grid__item').toArray().map($);
        let partialEvents = $events.reduce((acc: PartialEvent[], $event) => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Friedas Büxe';

            // Date
            let dateMatch = matchForce(
                $event.find('h6').textVisual(),
                /^(?<day>\d{1,2})\.(?<month>\d{1,2})\.(?<year>\d{4}) (?<hour>\d{1,2}):(?<minute>\d{2})$/u,
            );
            // The website says "0:00" but it's actually "24:00"
            if (parseInt(dateMatch.groups['hour']) == 0 && parseInt(dateMatch.groups['minute']) == 0) {
                dateMatch.groups['hour'] = '24';
            }
            event.time.from.setDateByParts(dateMatch.groups['day'], dateMatch.groups['month'], dateMatch.groups['year']);
            event.time.from.setTimeByParts(dateMatch.groups['hour'], dateMatch.groups['minute']);
            if (event.time.from.isInPast()) {
                return acc;
            }

            // URL
            let urlEvent = $event.find('a.jet-listing-dynamic-image__link').attr('href');
            event.links.push({label: 'Friedas Büxe', url: urlEvent});

            acc.push({url: urlEvent, event})
            return acc;
        }, []);
        return await Promise.all(partialEvents.map(partialEvent => this.fetchEvent(partialEvent)));
    }

    async fetchEvent(partialEvent: PartialEvent): Promise<EventData> {
        let event = partialEvent.event;
        let html = await fetchUrl(partialEvent.url);
        let $ = cheerio.load(html);
        let $event = $('.jupiterx-main')

        let title = $event.find('h1').textVisual();

        let text = $event.find('h3').textVisual();
        let lines = text
            .replace(/ *\n */g, '\n')
            .split(/\n+/)
            .filter(l => !l.match(/(Wohnzimmer|Salon):/i));
        event.description = joinColon([title, joinComma(lines)]);

        return event.build();
    }

}

type PartialEvent = { url: string, event: EventDataBuilder };
