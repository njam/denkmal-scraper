import {Friedasbuexe} from "./friedasbuexe";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Friedasbuexe', () => {

    test('Friedasbuexe-2021-08-07', async function () {
        await nockBack('Friedasbuexe-2021-08-07', async () => {
            let source = new Friedasbuexe();
            let events = await source.fetch(testFetchRequest('2021-08-07'));
            expect(events).toMatchSnapshot();
        });
    });

});
