import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";


export class Heldenbar implements Source {

    sourceName(): string {
        return `heldenbar`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Heldenbar()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://heldenbar.ch`;
        let url = `${urlBase}/programm/`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = $('#main-container div.vsel-upcoming').toArray().map($);
        let urls = $events.map($event => {
            return $event.find('.vsel-meta-title > a').attr('href');
        });
        return await Promise.all(urls.map(url => this.fetchEvent(url, request)));
    }

    async fetchEvent(url: string, request: FetchRequest): Promise<EventData> {
        let html = await fetchUrl(url);
        let $ = cheerio.load(html);
        let $event = $('#main-container article.event')

        let event = new EventDataBuilder(request.now, url);
        event.venueName = 'Provitreff';
        event.links.push({label: 'Heldenbar', url});
        event.description = $event.find('h1.entry-title').textVisual();

        let dateText = $event.find('.vsel-meta-date').textVisual();
        event.time.from.setByMatch(dateText, /^(?<day>\d{1,2})\/(?<month>\d{1,2})\/(?<year>\d{4})$/u);

        // Website: "Jeden Mittwoch ab 20 Uhr im Provitreff"
        event.time.from.setTime([20, 0]);

        let genre_categories = $event.find('.vsel-meta-cats a').toArray().map($)
            .map($link => $link.textVisual())
            .filter(categoryName => {
                if (/^\d{4}$/.test(categoryName)) {
                    // Ignore categories that are year numbers
                    return false
                }
                let categoryNotGenre = [
                    "Hinweis",
                    "Resident",
                    "Show",
                ]
                if (categoryNotGenre.includes(categoryName)) {
                    return false
                }
                return true
            })
        event.genres.add(genre_categories);

        return event.build();
    }

}
