import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinComma, joinDot, matchOptional} from "source/textHelper";
import {ParseError} from "error/parseError";
import {DenkmalRepo} from "denkmal_data/repo";


export class Provitreff implements Source {

    sourceName(): string {
        return `provitreff`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Provitreff()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://www.provitreff.ch/content/programm_kultur.html`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $table = $('table');
        if ($table.length != 1) {
            throw new ParseError(`Found ${$table.length} tables instead of 1.`, {html});
        }
        let $trs = $table.find('tr').toArray().map($);
        return $trs.map($tr => {
            let event = new EventDataBuilder(request.now, url);
            event.venueName = 'Provitreff';

            let $tds = $tr.find('td').toArray().map($);
            if ($tds.length != 2) {
                throw new ParseError(`Found ${$tds.length} TDs instead of 2.`, {html});
            }

            let textLeft = $tds[0].text();
            event.time.from.setByMatch(textLeft, /\b(?<day>\d{1,2})\.(?<month>\d{1,2})\.?/);

            let textRight = $tds[1].textVisual();
            let paragraphs = textRight.split(/\n\s*\n/).map(s => s.trim()).filter(s => s.length > 0);
            matchParagraphs(event, paragraphs, html);

            return event.build();
        });
    }
}

function matchParagraphs(event: EventDataBuilder, paragraphs: string[], html: string) {
    let descs: string[] = [];
    let title = paragraphs.shift();
    if (!title) {
        throw new ParseError(`No title paragraph found for event.`, {html});
    }
    let titleMatch = matchOptional(title, /^(?<hour>\d{1,2}):(?<minute>\d{2})\s*(Uhr)?\s+(?<title>.+)$/);
    if (titleMatch) {
        event.time.from.setTimeByParts(titleMatch.groups['hour'], titleMatch.groups['minute']);
        descs.push(titleMatch.groups['title']);
    } else {
        descs.push(title);
    }

    function getLinesFromList(para: string): string[] {
        let lines: string[] = para.split(/\n/).map(s => s.trim()).filter(s => s.length > 0);
        lines.shift();
        lines = lines.map(l => l.replace(/^-\s*/, ''));
        return lines;
    }

    for (let para of paragraphs) {
        if (para.match(/^Eintritt/)) {
            // ignore
        } else if (para.match(/^DJs/)) {
            let lines = getLinesFromList(para);
            if (lines.length > 0) {
                descs.push(joinComma(lines));
            } else {
                descs.push(para);
            }
        } else if (para.match(/^Musikstil/)) {
            let lines = getLinesFromList(para);
            if (lines.length > 0) {
                event.genres.add(lines);
            } else {
                descs.push(para);
            }
        } else {
            descs.push(para);
        }
    }

    event.description = joinDot(descs);
}
