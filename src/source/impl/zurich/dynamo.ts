import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinDot} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";


export class Dynamo implements Source {

    sourceName(): string {
        return `dynamo`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Dynamo()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let eventsByCategory = await Promise.all([
            this.fetchCategory(1, request), // Konzerte
            this.fetchCategory(2, request), // Parties
        ]);
        return eventsByCategory.reduce((acc, cur) => acc.concat(cur), []);
    }

    async fetchCategory(eventType: number, request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://www.dynamo.ch`;
        let url = `${urlBase}/veranstaltungen?field_event_type_tid=${eventType}`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = $('.view-events .node-event').toArray().map($);
        return $events.map($event => {
            let eventUrl = $event.find('.field-name-title a').attr('href');
            if (!eventUrl.startsWith('http')) {
                eventUrl = urlBase + eventUrl;
            }
            let event = new EventDataBuilder(request.now, eventUrl);
            event.venueName = 'Dynamo';
            event.links.push({label: 'Dynamo', url: eventUrl});

            let $date = $event.find('.field-name-field-event-zeitraum > *');
            if ($date.hasClass('date-display-range')) {
                event.time.from.setByMatch($date.find('.date-display-start').textVisual(),
                    /^(?<weekday>\p{L}+), (?<day>\d{1,2})\. (?<month>\p{L}+) (?<year>\d{4}) - (?<hour>\d{1,2}):(?<minute>\d{2})$/u
                );
                event.time.until.setByMatch($date.find('.date-display-end').textVisual(),
                    /^(?<weekday>\p{L}+), (?<day>\d{1,2})\. (?<month>\p{L}+) (?<year>\d{4}) - (?<hour>\d{1,2}):(?<minute>\d{2})$/u
                );
            } else {
                let $timeRange = $date.find('.date-display-range').remove();
                if ($timeRange.length > 0) {
                    event.time.from.setByMatch($date.textVisual(),
                        /\b(?<weekday>\p{L}+), (?<day>\d{1,2})\. (?<month>\p{L}+) (?<year>\d{4})\b/u
                    );
                    event.time.from.setByMatch($timeRange.find('.date-display-start').textVisual(),
                        /^(?<hour>\d{1,2}):(?<minute>\d{2})$/u
                    );
                    event.time.until.setByMatch($timeRange.find('.date-display-end').textVisual(),
                        /^(?<hour>\d{1,2}):(?<minute>\d{2})$/u
                    );
                } else {
                    event.time.from.setByMatch($date.textVisual(),
                        /^(?<weekday>\p{L}+), (?<day>\d{1,2})\. (?<month>\p{L}+) (?<year>\d{4}) - (?<hour>\d{1,2}):(?<minute>\d{2})$/u
                    );
                }
            }

            let title = $event.find('.field-name-title h2').textVisual();
            let subtitle = $event.find('.field-name-field-event-subtitel').textVisual();
            event.description = joinDot([title, subtitle]);

            return event.build();
        });
    }

}
