import {Heldenbar} from "./heldenbar";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Heldenbar', () => {

    test('heldenbar-2022-11-17', async function () {
        await nockBack('heldenbar-2022-11-17', async () => {
            let source = new Heldenbar();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });

});
