import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {Moment} from "moment-timezone";
import cheerio from "cheerio";
import {joinColon, matchForce} from "source/textHelper";

export class Usgang implements Source {
    private _regionVariant: RegionVariant;
    private locationId: string;

    constructor(regionVariant: RegionVariant, locationId: string) {
        this._regionVariant = regionVariant;
        this.locationId = locationId;
    }

    sourceName(): string {
        return `usgang-${this.locationId}`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.ListingWebsite;
    }

    regionVariant(): RegionVariant {
        return this._regionVariant;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [
            new Usgang(RegionVariant.zurich, '11848'),  // Friedas Büxe
            new Usgang(RegionVariant.zurich, '12573'),  // Gonzo
        ];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `${this.baseUrl()}/locationdetail.php?locationid=${this.locationId}`;
        let html = await fetchUrl(url);
        let $ = cheerio.load(html);

        let $eventsTable = $('#main > table:nth-child(2) > tbody > tr:nth-child(1) > td:nth-child(1) > table');
        let eventIds = $eventsTable.find('td a').toArray()
            .map(el => $(el).attr('href'))
            .map(url => matchForce(url, /\?eventid=(\d+)$/)[1]);

        return await Promise.all(
            eventIds.map(eventId => this._fetchEvent(request.now, eventId))
        );
    }

    private async _fetchEvent(now: Moment, eventId: String): Promise<EventData> {
        let url = `${this.baseUrl()}/eventdetail.php?eventid=${eventId}`;
        let html = await fetchUrl(url);
        let $ = cheerio.load(html);

        let $table = $('#main table.vevent');
        let title = $table.find('.inhalttitel > h1').textVisual();

        let $details = $table.find('table > tbody > tr');
        let details: { [key: string]: Cheerio } = {};
        for (let $tr of $details.toArray().map($)) {
            let $tds = $tr.find('> td').toArray().map($);
            if ($tds.length === 2) {
                details[$tds[0].textVisual().toLocaleLowerCase()] = $tds[1];
            }
        }

        let event = new EventDataBuilder(now, url);
        event.venueName = details['wo'].find('a').textVisual();
        event.time.from.setByISO8601(details['wann'].find('.value-title').attr('title'));
        event.description = details['acts'] == undefined ? title : joinColon([title, details['acts'].textVisual()]);
        if (details['musikstil']) {
            event.genres.add(
                details['musikstil'].textVisual().replace(/ und /, ', ')
            );
        }

        return event.build();
    }

    private baseUrl(): string {
        let cities = {
            [RegionVariant.zurich]: 'zuerich',
            [RegionVariant.basel]: 'basel',
        };
        let regionVariant = this.regionVariant();
        if (!cities.hasOwnProperty(regionVariant)) {
            throw new Error(`Unsupported region variant: ${regionVariant}`);
        }
        let city = cities[regionVariant];
        return `https://${city}.usgang.ch`;
    }

}
