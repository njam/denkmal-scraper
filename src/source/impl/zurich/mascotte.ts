import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {joinComma} from "source/textHelper";
import {DenkmalRepo} from "denkmal_data/repo";


export class Mascotte implements Source {

    sourceName(): string {
        return `mascotte`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Mascotte()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://www.mascotte.ch/nu/events/event_list_type/2`;
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $events = $('.nu-event').toArray().map($);
        return $events.map($event => {
            let eventUrl = $event.find('.nu-e-link-share').attr('data-a2a-url');
            let event = new EventDataBuilder(request.now, eventUrl);
            event.venueName = 'Mascotte';
            event.links.push({label: 'mascotte', url: eventUrl});

            let date = $event.find('.mobi-only .nu-e-date').textVisual();
            event.time.from.setByMatch(date, /\b(?<day>\d{1,2})\.(?<month>\d{1,2})\.(?<year>\d{2})\b/u);

            let time = $event.find('.mobi-only .nu-e-time-age').textVisual();
            event.time.from.setByMatch(time, /\b(?<hour>\d{1,2})\.(?<minute>\d{2})\b/u);

            let title = $event.find('.mobi-only h2').textVisual();
            let opening_band = $event.find('.mobi-only .nu-e-opening-band').textVisual();
            event.description = joinComma([title, opening_band]);

            return event.build();
        });
    }

}
