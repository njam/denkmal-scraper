import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import * as de from "@mojotech/json-type-validation";
import {Decoder} from "@mojotech/json-type-validation";


export class Moods implements Source {

    sourceName(): string {
        return `moods`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.zurich;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Moods()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let urlBase = `https://www.moods.ch`;
        let html = await fetchUrl(`${urlBase}/de/program`);
        let $ = cheerio.load(html);

        let jsonStr = $('script#__NEXT_DATA__').html();
        let events = this._parseNextJson(jsonStr || '');

        return events
            .flatMap((eventJson) => {
                if (!eventJson.isVisible) {
                    return [];
                }

                let eventUrl = `${urlBase}/${eventJson.slug}`;
                let event = new EventDataBuilder(request.now, eventUrl);
                event.venueName = 'Moods';
                event.links.push({label: event.venueName, url: eventUrl});

                // Date and time
                event.time.from.setByMatch(
                    eventJson.eventDate,
                    /^(?<year>\d{4})-(?<month>\d{1,2})-(?<day>\d{1,2})$/u,
                );
                event.time.from.setByMatch(
                    eventJson.timeStart,
                    /^(?<hour>\d{1,2}):(?<minute>\d{2})$/u,
                );
                if (event.time.from.isAfter(request.dateMax())) {
                    return [];
                }

                // Description
                event.description = eventJson.name;
                if (eventJson.series != "") {
                    event.description = `${eventJson.series}: ${event.description}`;
                }

                return [event.build()];
            });
    }

    _parseNextJson(jsonStr: string): EventType[] {
        let json = JSON.parse(jsonStr);
        let jsonEvents = json['props']['pageProps']['fallback']['allEvents'];

        let decoder: Decoder<EventType[]> = de.array(de.object({
            isVisible: de.boolean(),
            series: de.string(),
            name: de.string(),
            eventDate: de.string(),
            timeStart: de.string(),
            slug: de.string(),
            bands: de.array(de.object({
                name: de.string(),
            })),
        }));

        return decoder.runWithException(jsonEvents);
    }
}

type EventType = {
    isVisible: boolean,
    series: string,
    name: string,
    eventDate: string,
    timeStart: string,
    slug: string,
    bands: {
        name: string,
    }[],
};

