import cheerio from "cheerio";
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";


export class SquatRadar implements Source {
    private _regionVariant: RegionVariant;
    private city: string;


    constructor(regionVariant: RegionVariant, city: string) {
        this._regionVariant = regionVariant;
        this.city = city;
    }

    sourceName(): string {
        return `squatradar-city:${this.city}`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.ListingWebsite;
    }

    regionVariant(): RegionVariant {
        return this._regionVariant;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [
            new SquatRadar(RegionVariant.zurich, 'Zurich'),
        ];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://radar.squat.net/en/events/category/music-concert/city/${this.city}`;
        let html = await fetchUrl(url);

        function cleanupText(text: string): string {
            text = text.replace(/::$/, '');
            return text;
        }

        let $ = cheerio.load(html);
        let articles = Array.from($('article[typeof="Event"].confirmed'));
        return articles.map(article => {
            let eventUrl = $(article).find('.event-list-event-title a').attr('href');
            let event = new EventDataBuilder(request.now, eventUrl);

            event.description = cleanupText($(article).find('.event-list-event-title').textVisual());

            let placeName = $(article).find('[typeof="Place"] [property="name"]').textVisual();
            let organizationName = $(article).find('[typeof="Organization"] [property="schema:name"]').textVisual();
            event.venueName = placeName || organizationName;

            let timeText = $(article).find('[property="schema:startDate"]').attr('content');
            event.time.from.setByISO8601(timeText);

            return event.build();
        });
    }

}
