import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import * as de from "@mojotech/json-type-validation";
import {joinComma} from "source/textHelper";


export class Stadtkonzerte implements Source {
    private _regionVariant: RegionVariant;
    private city: string;


    constructor(regionVariant: RegionVariant, city: string) {
        this._regionVariant = regionVariant;
        this.city = city;
    }

    sourceName(): string {
        return `stadtkonzerte-city:${this.city}`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.ListingWebsite;
    }

    regionVariant(): RegionVariant {
        return this._regionVariant;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [
            new Stadtkonzerte(RegionVariant.zurich, 'Zürich'),
            new Stadtkonzerte(RegionVariant.basel, 'Basel'),
        ];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `https://stadtkonzerte.ch/gigs.json`;
        let text = await fetchUrl(url);
        let events = this.parseResponse(text);

        return events
            .filter(e => e.c_name == this.city)
            .map(e => {
                let sourceUrl = `https://stadtkonzerte.ch/konzerte?region=${this.city}`;
                let event = new EventDataBuilder(request.now, sourceUrl);
                event.venueName = e.l_name;
                event.time.from.setByMatch(e.date, /(?<year>\d{4})-(?<month>\d{1,2})-(?<day>\d{1,2})/u);
                event.time.from.setTime([20, 0]);
                event.description = joinComma(e.artists);
                if (e.tag.length > 0) {
                    event.description += ` - ${e.tag}`;
                }
                if (e.g1) {
                    event.genres.add(e.g1);
                }
                return event.build();
            });
    }

    private parseResponse(body: string): EventNodeType[] {
        let json = JSON.parse(body);

        let decodeArray: de.Decoder<{}[]> = de.array(de.object());

        let decodeItem: de.Decoder<{ _source: EventNodeType }> = de.object({
            _type: de.constant('gig'),
            _source: de.object().andThen<EventNodeType>(obj => {
                let artistKeys = Object.keys(obj).filter(f => f.match(/^a(\d+)?_(name|pre)/));
                let artists: string[] = [];
                for (let artistKey of artistKeys) {
                    try {
                        artists.push(de.string().runWithException(obj[artistKey]));
                    } catch (e) {
                        return de.fail(`Failed to parse artist in '${artistKey}': ${e.message}`)
                    }
                }
                if (artists.length === 0) {
                    return de.fail(`Event has no artists`);
                }
                obj.artists = artists;

                return de.object({
                    c_name: de.optional(de.string()),
                    l_name: de.string(),
                    date: de.string(),
                    g1: de.optional(de.string()),
                    tag: de.string(),
                    artists: de.array(de.string()),
                });
            }),
        });

        return decodeArray.runWithException(json)
            .map(item => decodeItem.runWithException(item))
            .map(e => e._source);
    }
}


type EventNodeType = {
    c_name?: string,
    l_name: string,
    date: string,
    g1?: string,
    tag: string,
    artists: string[],
};
