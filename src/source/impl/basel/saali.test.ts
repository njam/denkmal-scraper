import moment from 'moment-timezone';
import {nockBack} from "test/nockBack";
import {Saali} from "./saali";
import {testFetchRequest} from "test/testFetchRequest";

describe('Sääli', () => {
    test('Sääli-2019-10-21', async () => {
        await nockBack('sääli-2019-10-21', async () => {
            let source = new Saali();
            let events = await source.fetch(testFetchRequest('2019-10-21'));
            expect(events).toMatchSnapshot();
        });
    });
    test('Sääli-2019-10-29', async () => {
        await nockBack('sääli-2019-10-29', async () => {
            let source = new Saali();
            let events = await source.fetch(testFetchRequest('2019-10-29'));
            expect(events).toMatchSnapshot();
        });
    });

    test('Sääli-2019-12-24', async () => {
        await nockBack('sääli-2019-12-24', async () => {
            let source = new Saali();
            let events = await source.fetch(testFetchRequest('2019-12-24'));
            expect(events).toMatchSnapshot();
        });
    });

    test('Sääli-2020-03-15', async () => {
        await nockBack('sääli-2020-03-15', async () => {
            let source = new Saali();
            let events = await source.fetch(testFetchRequest('2020-03-15'));
            expect(events).toMatchSnapshot();
        });
    });

    test('Sääli-2020-06-06', async () => {
        await nockBack('sääli-2020-06-06', async () => {
            let source = new Saali();
            let events = await source.fetch(testFetchRequest('2020-06-06'));
            expect(events).toMatchSnapshot();
        });
    });

    test('Sääli-2022-11-17', async () => {
        await nockBack('sääli-2022-11-17', async () => {
            let source = new Saali();
            let events = await source.fetch(testFetchRequest('2022-11-17'));
            expect(events).toMatchSnapshot();
        });
    });
});
