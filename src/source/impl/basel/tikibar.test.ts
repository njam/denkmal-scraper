import {Tikibar} from "./tikibar";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";


describe('Tikibar', () => {

    test('tikibar-2022-09-03', async function () {
        await nockBack('tikibar-2022-09-03', async () => {
            let source = new Tikibar();
            let events = await source.fetch(testFetchRequest('2022-09-03'));
            expect(events).toMatchSnapshot();
        });
    });

});
