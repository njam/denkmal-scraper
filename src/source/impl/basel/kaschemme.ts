import {Source, SourcePriority} from "source/source";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {FetchRequest} from "runner";
import {EventData, EventDataBuilder} from "source/eventData";
import {fetchUrl} from "../../../fetchUrl";
import cheerio from "cheerio";
import 'cheerio/text';

export class Kaschemme implements Source {
    sourceName(): string {
        return "kaschemme";
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Kaschemme()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let baseUrl = 'https://www.kaschemme.ch';
        let pageUrl = `${baseUrl}/programm`;
        let html = await fetchUrl(pageUrl);

        let $ = cheerio.load(html);
        let $events = $('.eventlist-event--upcoming').toArray().map($);
        return $events.map($event => {
            let event = new EventDataBuilder(request.now, pageUrl);
            event.venueName = 'Kaschemme';
            const $title = $event.find('.eventlist-title-link');
            event.description = $title.textVisual();
            let url: string = $title.attr('href');
            if (url.startsWith('/')) {
                url = `${baseUrl}${url}`
            }
            event.links.push({label: event.venueName, url: url});

            let dateRe: RegExp = /(?<year>\d{4})-(?<month>\d{1,2})-(?<day>\d{1,2})/;
            let timeRe: RegExp = /(?<hour>\d{1,2}):(?<minute>\d{2})/;
            let $eventTime = $event.find('.event-time-24hr');
            let $startTime = $eventTime.find('.event-time-24hr-start').first();
            if ($startTime.length === 1) {
                // until time on same day
                event.time.from.setByMatch($startTime.attr('datetime'), dateRe);
                event.time.from.setByMatchOptional($startTime.text(), timeRe);

                const $endTime = $eventTime.find('.event-time-12hr-end').first();
                if ($endTime.length === 1 && $endTime.text() != $startTime.text()) {
                    event.time.until.setByMatch($endTime.attr('datetime'), dateRe);
                    event.time.until.setByMatchOptional($endTime.text(), timeRe);
                }
            } else {
                // until time on next day
                const $fromTime = $eventTime.first();
                event.time.from.setByMatch($fromTime.attr('datetime'), dateRe);
                event.time.from.setByMatchOptional($fromTime.text(), timeRe);

                if ($eventTime.length === 2) {
                    const $untilTime = $eventTime.last();
                    event.time.until.setByMatch($untilTime.attr('datetime'), dateRe);
                    event.time.until.setByMatchOptional($untilTime.text(), timeRe);
                }
            }

            const bannedWords = ['eintritt', 'konzert', 'kinderdisko'];
            const $genres = $event.find('.eventlist-cats a').toArray().map($);
            event.genres.add(
                $genres.map(($genre: Cheerio) => {
                    return $genre.text().toLocaleLowerCase().trim();
                }).filter((tag: string) => {
                    for (const word of bannedWords) {
                        if (tag.includes(word)) {
                            return false;
                        }
                    }
                    return true;
                })
            );

            return event.build();
        });

    }
}