import cheerio from "cheerio";
import 'cheerio/text'
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {joinColon, joinComma} from "source/textHelper";

export class Saali implements Source {

    sourceName(): string {
        return "sääli";
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Saali()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = 'https://www.goldenes-fass.ch/saali/';
        let html = await fetchUrl(url);
        let $ = cheerio.load(html);

        if ($(".entry-content strong:contains('Weiteres keine Veranstaltungen')").length > 0) {
            return Promise.resolve([]);
        }

        let $paragraphs = $('.entry-content > p').toArray().map($);

        // Split off paragraphs at the first non-agenda item
        let indexParagraphEnd = $paragraphs.findIndex(($p) => $p.text().includes("Unsere Hauslabels"));
        if (indexParagraphEnd > 0) {
            $paragraphs = $paragraphs.slice(0, indexParagraphEnd);
        }

        let $eventParagraphs = $paragraphs.filter(($p) => {
            if ($p.find('.simple-icon-facebook').length > 0) {
                return false;
            }

            if ($p.text().includes('PROGRAMM SÄÄLI')) {
                return false;
            }
            return $p.text().trim().length > 0;
        });

        let events: EventData[] = [];
        for (let $p of $eventParagraphs) {
            let lines = $p.textVisual().split(/\n/);

            let closedRe: RegExp = /_+(?<day>\d{1,2})_+(?<month>\d{1,2})_*[A-Za-z\u00E4-\u00F6\u00FC ]*geschlossen/;
            if (closedRe.test(lines[0])) {
                continue;
            }

            let dateRe: RegExp = /^\w{2,10}_+(?<day>\d{1,2})_+(?<month>\d{1,2})_+(?<hour>\d{1,2})h?_*/;

            let event = new EventDataBuilder(request.now, url);
            event.time.from.setByMatch(lines[0], dateRe);
            event.venueName = "Sääli";

            let title = lines[0].replace(dateRe, '');
            lines.shift();

            let description = joinColon([title, joinComma(lines)]);

            if (description.length == 0) {
                continue;
            }

            event.description = description;

            let $link = $p.find('a').first().attr('href');
            if ($link) {
                event.links.push({label: title, url: $link});
            }
            events.push(event.build());
        }

        return events;
    }
}
