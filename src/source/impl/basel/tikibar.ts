import cheerio from 'cheerio';
import 'cheerio/text';
import {EventData, EventDataBuilder} from 'source/eventData';
import {fetchUrl, getHtml} from "fetchUrl";
import {Source, SourcePriority} from 'source/source'
import {FetchRequest} from "runner";
import {RegionVariant} from "denkmal_data/region";
import {DenkmalRepo} from "denkmal_data/repo";
import {ParseError} from "error/parseError";
import {joinColon, replaceForce, stringIncludesAnyCase} from "source/textHelper";

export class Tikibar implements Source {

    private urlBase: string

    constructor() {
        this.urlBase = `https://tiki-bar.ch/`;
    }

    sourceName(): string {
        return `tikibar`;
    }

    sourcePriority(): SourcePriority {
        return SourcePriority.VenueWebsite;
    }

    regionVariant(): RegionVariant {
        return RegionVariant.basel;
    }

    loadInstances(repo: DenkmalRepo): Source[] {
        return [new Tikibar()];
    }

    async fetch(request: FetchRequest): Promise<EventData[]> {
        let url = `${this.urlBase}/`;
        let html = await fetchUrl(url);
        let $ = cheerio.load(html);

        // Get list of event pages
        let eventUrls = $('div#posts > article .preview-title a').toArray().map($)
            .map($eventLink => $eventLink.attr('href'))
            .map(url => {
                if (!url.startsWith('http')) {
                    url = this.urlBase + url;
                }
                return url;
            });

        // Retrieve each event page
        return await Promise.all(eventUrls.map(async eventUrl => {
            return await this._fetchEvent(eventUrl, request)
        }));
    }

    async _fetchEvent(url: string, request: FetchRequest): Promise<EventData> {
        let html = await fetchUrl(url);

        let $ = cheerio.load(html);
        let $event = $('main > article');


        let event = new EventDataBuilder(request.now, url);
        event.venueName = 'Tiki-Bar';

        let titleText = $event.find('.entry-title').textVisual();
        event.description = replaceForce(titleText, /^\d{1,2}\.\d{1,2}\.: /, '')

        event.time.from.setByMatch(
            $event.find('.entry-content').textVisual(),
            /(?<weekday>\p{L}+), (?<day>\d{1,2}). (?<month>\p{L}+), (?<year>\d{4}), (?<hour>\d{1,2})(\.(?<minute>\d{2}))?h/u,
        );

        event.links.push({label: 'tikibar', url});

        return event.build();
    }

}
