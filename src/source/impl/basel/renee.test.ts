import {Renee} from "./renee";
import {nockBack} from "test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Renée', () => {
    test('renee-2022-11-23', async () => {
        await nockBack('renee-2022-11-23', async () => {
            let source = new Renee();
            let events = await source.fetch(testFetchRequest('2022-11-23'));
            expect(events).toMatchSnapshot();
        });
    });
});
