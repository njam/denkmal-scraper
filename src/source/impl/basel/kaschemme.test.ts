import moment from 'moment-timezone';
import {Kaschemme} from "./kaschemme";
import {nockBack} from "../../../test/nockBack";
import {testFetchRequest} from "test/testFetchRequest";

describe('Kaschemme', () => {
    test('Kaschemme-2019-09-15', async () => {
        await nockBack('kascheme-2019-09-15', async () => {
            let source = new Kaschemme();
            let events = await source.fetch(testFetchRequest('2019-09-15'));
            expect(events).toMatchSnapshot();
        });
    });

    test('Kaschemme until date same', async () => {
        await nockBack('kascheme-2019-10-21', async () => {
            let source = new Kaschemme();
            let events = await source.fetch(testFetchRequest('2019-10-21'));
            expect(events).toMatchSnapshot();
        });
    });
});
