import {Stadtkonzerte} from "./stadtkonzerte";
import {RegionVariant} from "denkmal_data/region";
import {testFetchRequest} from "test/testFetchRequest";
import {nockBack} from "test/nockBack";

describe('stadtkonzerte', () => {

    test('stadtkonzerte-zurich-2022-11-19', async () => {
        await nockBack('stadtkonzerte-2022-11-19', async () => {
            let source = new Stadtkonzerte(RegionVariant.zurich, 'Zürich');
            let events = await source.fetch(testFetchRequest('2022-11-19'));
            expect(events).toMatchSnapshot();
        });
    });

});
