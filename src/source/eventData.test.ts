import {EventDataBuilder} from "./eventData";
import moment from "moment-timezone";

describe('EventDataBuilder', () => {
    let builder: EventDataBuilder;
    beforeEach(() => {
        let now = moment.tz('2018-12-16T12:00:00', 'Europe/Zurich');
        builder = new EventDataBuilder(now, 'http://my-source.com');
    });

    test('build', () => {
        builder.venueName = "My Venue";
        builder.description = "my description";
        builder.time.from.setByISO8601('2018-12-16 20:30');
        builder.time.until.setByISO8601('2018-12-16 23:00');
        builder.genres.add("my-tag");
        builder.links = [{label: "my-link", url: "http://example.com"}];

        let event = builder.build();
        expect(event).toMatchObject({
            sourceUrl: "http://my-source.com",
            venueName: "My Venue",
            description: "My description",
            time: {
                from: {date: [2018, 12, 16], time: [20, 30]},
                until: {date: [2018, 12, 16], time: [23, 0]},
            },
            genres: ["my-tag"],
            links: [{label: "My-link", url: "http://example.com"}],
        });
    });

    test('build with optional fields', () => {
        builder.venueName = "My Venue";
        builder.description = "My Description";
        builder.time.from.setByISO8601('2018-12-16 20:00');

        let event = builder.build();
        expect(event).toMatchObject({
            venueName: "My Venue",
            description: "My Description",
            time: {
                from: {date: [2018, 12, 16], time: [20, 0]},
            },
            genres: [],
            links: [],
        });
    });

    test('build with missing fields', () => {
        expect(() => builder.build()).toThrow(`Missing 'venueName'`);
    });

    test('build with cleanup', () => {
        builder.venueName = "My Venue";
        builder.description = " MEINE BESCHREIBUNG:: Coole party. DJ`s EISBÆR, dj Fux, BÄRTSCH’S RONIN    und Bar [Techno, Cookin‘ Soul, ROCK'N`ROLL].   ";
        builder.time.from.setByISO8601('2018-12-16 20:00');

        let event = builder.build();
        expect(event).toMatchObject({
            description: "Meine Beschreibung: Coole party. DJs Eisbær, DJ Fux, Bärtsch's Ronin und Bar (Techno, Cookin' Soul, Rock'n'Roll)",
        });
    });

    test('build with cleanup - keep dot after number', () => {
        builder.venueName = "My Venue";
        builder.description = "Findet statt am 12.";
        builder.time.from.setByISO8601('2018-12-16 20:00');

        let event = builder.build();
        expect(event).toMatchObject({
            description: "Findet statt am 12.",
        });
    });


});
