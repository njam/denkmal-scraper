import winston from "winston";
import WinstonElasticsearch from "winston-elasticsearch";

export {Logger, LoggerOptions, Level, AbstractLogger, WinstonLogger, MockLogger, MockLoggerEntry};

enum Level {
    error = 'error',
    warn = 'warn',
    info = 'info',
    debug = 'debug',
}

type Fields = { [key: string]: string | number | Error };

type LoggerOptions = {
    console: {
        level: string;
    }
    elasticsearch?: {
        level: string;
        host: string;
    }
};

interface Logger {
    log(level: Level, message: string, fields?: Fields): void;

    // Unexpected error that should be investigated by a human.
    error(message: string, fields?: Fields): void;

    // Expected error, e.g. due to network problems.
    warn(message: string, fields?: Fields): void;

    // Important message about the state of the program.
    info(message: string, fields?: Fields): void;

    // Less important message, merely interesting for debugging/developing the program.
    debug(message: string, fields?: Fields): void;
}

abstract class AbstractLogger implements Logger {
    abstract log(level: Level, message: string, fields?: Fields): void;

    error(message: string, fields?: Fields) {
        this.log(Level.error, message, AbstractLogger.parseFields(fields));
    }

    warn(message: string, fields?: Fields) {
        this.log(Level.warn, message, AbstractLogger.parseFields(fields));
    }

    info(message: string, fields?: Fields) {
        this.log(Level.info, message, AbstractLogger.parseFields(fields));
    }

    debug(message: string, fields?: Fields) {
        this.log(Level.debug, message, AbstractLogger.parseFields(fields));
    }

    private static parseFields(fields?: Fields): Fields {
        fields = fields || {};
        if (fields.error && fields.error instanceof Error) {
            let error: any = fields['error'];
            delete (fields['error']);
            if (error.name) {
                fields['error_name'] = error.name;
            }
            if (error.message) {
                fields['error_message'] = error.message;
            }
            if (error.stack) {
                fields['error_stack'] = error.stack;
            }
            if (error.extra && typeof error.extra === 'object') {
                for (let k in error.extra) {
                    fields[`error_${k}`] = error.extra[k];
                }
            }
        }
        return fields;
    }
}

class WinstonLogger extends AbstractLogger {
    private fields: Fields;
    private winston: winston.Logger;

    constructor(options: LoggerOptions, fields?: Fields) {
        super();
        this.fields = fields || {};

        let transportConsole = new winston.transports.Console({
            level: options.console.level,
            format: winston.format.cli(),
        });

        let transportElasticsearch: any;
        if (options.elasticsearch) {
            transportElasticsearch = new WinstonElasticsearch({
                level: options.elasticsearch.level,
                indexPrefix: 'winston',
                clientOpts: {
                    host: options.elasticsearch.host,
                    // Disable SSL verification, to avoid error "Unable to load PFX certificate"
                    // See https://stackoverflow.com/q/68082679/3090404
                    ssl: {
                        pfx: [],
                        rejectUnauthorized: false
                    }
                },
            });
        }

        this.winston = winston.createLogger({
            transports: [transportConsole, transportElasticsearch].filter(t => !!t)
        });
    }

    async shutdown() {
        this.winston.end();
        await new Promise((resolve, reject) => {
            this.winston.on('finish', resolve);
        });
    }

    log(level: Level, message: string, fields?: Fields) {
        fields = Object.assign(fields || {}, this.fields);
        this.winston.log(level, message, fields);
    }

}

type MockLoggerEntry = { level: Level, message: string, fields: object };

class MockLogger extends AbstractLogger {
    private _entries: MockLoggerEntry[] = [];

    log(level: Level, message: string, fields?: object): void {
        fields = fields || {};
        this._entries.push({level, message, fields});
    }

    entries(level?: Level): MockLoggerEntry[] {
        let entries = Array.from(this._entries);
        if (level) {
            entries = entries.filter(e => e.level == level);
        }
        return entries;
    }
}
