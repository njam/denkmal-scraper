import commander from 'commander';
import {Config, loadConfig} from "config";
import {Runner} from "runner";
import {Logger, WinstonLogger} from "logger";
import {DenkmalApi} from "denkmal_api/client";
import {loadSources} from "source/loadSources";
import {DenkmalRepo} from "denkmal_data/repo";
import {Venue} from "denkmal_data/venue";
import {sleep} from "sleep";

commander
    .name('scraper')
    .option('-c, --config <file>', 'Path to config file', 'config/config.toml')
    .option('--one-shot', 'Run once, then exit (instead of running indefinitely).', false)
    .option('--dry-run', 'Only fetch, but don\'t upload events.', false)
    .option('--filter <source-name>', 'Filter by source name (e.g. "xtra").')
    .parse(process.argv);

async function scraper(
    config: Config,
    logger: Logger,
    oneShot: boolean,
    dryRun: boolean,
    sourceFilter?: string,
) {
    let api = new DenkmalApi(config.denkmal_api.url, config.denkmal_api.auth_token);
    let venues: Venue[] = await api.getVenues();
    let repo = new DenkmalRepo(config.regions, venues);
    let sources = loadSources(repo, sourceFilter);
    let runner = new Runner(logger, sources, api, repo);
    // noinspection InfiniteLoopJS
    while (true) {
        let fetchResults = await runner.fetch(config.days_to_fetch);
        if (!dryRun) {
            await runner.upload(fetchResults);
        }
        if (oneShot) {
            break;
        }
        logger.debug(`Sleeping for ${config.run_every.humanize()}...`);
        await sleep(config.run_every.asSeconds());
    }
}

async function main() {
    let config = loadConfig(commander.config);
    let logger = new WinstonLogger(config.logging, {program: 'denkmal-scraper'});
    logger.info(`Denkmal-scraper is starting.`);
    await scraper(config, logger, commander.oneShot, commander.dryRun, commander.filter);
    logger.info(`Denkmal-scraper is exiting.`);
    await logger.shutdown();
}

main().catch(e => {
    console.error(e);
    process.exit(1);
});
