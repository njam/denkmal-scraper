import got from "got";
import {HttpError} from "error/httpError";
import cheerio from "cheerio";

export {fetchUrl, getHtml}

async function getHtml(url: string): Promise<Cheerio> {
    return cheerio.load(await fetchUrl(url)).root();
}

async function fetchUrl(url: string, options: FetchOptions = {}): Promise<string> {
    let optionsGot: { [key: string]: any } = {
        timeout: 1000 * 30,
        retry: {
            retries: (iteration: number, error: any) => {
                if (iteration >= 3) {
                    return 0;
                }
                return 1;
            },
        },
        headers: {
            'User-Agent': 'Mozilla/5.0 AppleWebKit',
        },
    };
    if (options.method) {
        optionsGot['method'] = options.method;
    }
    if (options.body) {
        optionsGot['body'] = options.body;
    }
    if (options.headers) {
        optionsGot['headers'] = Object.assign(optionsGot['headers'], options.headers);
    }
    try {
        let response = await got(url, optionsGot);
        return response.body;
    } catch (e) {
        let extra: { [key: string]: string } = {};
        if (e.response) {
            extra['responseStatus'] = [e.response.statusCode, e.response.statusMessage].filter(s => !!s).join(' ');
            extra['responseBody'] = e.response.body;
        }
        throw new HttpError(`Failed to fetch '${url}': ${e.message}`, extra);
    }
}

type FetchOptions = {
    method?: 'get' | 'post';
    body?: string;
    headers?: { [key: string]: string },
};
