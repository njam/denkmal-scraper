# This file was generated based on ".graphqlconfig". Do not edit manually.

schema {
    query: Query
    mutation: Mutation
}

type Authentication {
    token: String
    user: User
}

type Event {
    createdAt: Date!
    description: String!
    eventDay: EventDay!
    from: Date!
    genres: [Genre]!
    hasTime: Boolean!
    id: ID!
    isHidden: Boolean!
    isPromoted: Boolean!
    isReviewPending: Boolean!
    links: [EventLink]!
    regionId: ID!
    until: Date
    variants: [EventVariant]!
    venue: Venue!
    venueId: ID!
}

type EventLink {
    label: String!
    url: URL!
}

type EventVariant {
    createdAt: Date!
    description: String!
    from: Date!
    genres: [Genre]!
    hasTime: Boolean!
    id: ID!
    isReviewPending: Boolean!
    links: [EventLink]!
    until: Date
}

type EventsList {
    count: Int!
    events: [Event]!
}

type Genre {
    category: GenreCategory!
    categoryId: ID!
    createdAt: Date!
    id: ID!
    name: String!
    updatedAt: Date!
}

type GenreCategoriesList {
    count: Int!
    genreCategories: [GenreCategory]!
}

type GenreCategory {
    color: String
    createdAt: Date!
    genres: [Genre]!
    id: ID!
    name: String!
    regionId: ID!
    updatedAt: Date!
}

type GenresList {
    count: Int!
    genres: [Genre]!
}

type Mutation {
    addEvent(description: String!, from: Date!, genres: [ID], hasTime: Boolean, id: ID, isHidden: Boolean, isPromoted: Boolean, links: [EventLinkInput], until: Date, venueId: ID!): Event
    addGenre(genreCategoryId: ID!, id: ID, name: String!): Genre
    addGenreCategory(color: String, id: ID, name: String!, regionId: ID!): GenreCategory
    addRegion(dayOffset: Float, email: String!, facebookPage: String, footerUrl: URL, id: ID, latitude: Float!, longitude: Float!, name: String!, slug: String!, suspendedUntil: Date, timeZone: String!, twitterAccount: String): Region
    addScraperEvents(events: [ScraperEventInput]!): [Boolean]
    addUser(email: String!, id: ID, isSuperuser: Boolean, name: String!, password: password!, regionId: ID): User
    addVenue(address: String, email: String, facebookPageId: String, id: ID, ignoreScraper: Boolean, isReviewPending: Boolean, isSuspended: Boolean, latitude: Float, longitude: Float, name: String!, regionId: ID!, twitter: String, url: URL): Venue
    changeGenre(genreCategoryId: ID, id: ID, name: String): Genre
    changeGenreCategory(color: String, id: ID!, name: String): GenreCategory
    changeRegion(dayOffset: Float, email: String, facebookPage: String, footerUrl: URL, id: ID!, latitude: Float, longitude: Float, name: String, slug: String, suspendedUntil: Date, timeZone: String, twitterAccount: String): Region
    changeUser(email: String, id: ID!, isSuperuser: Boolean, name: String, password: password, regionId: ID): User
    changeVenue(address: String, aliases: [String], email: String, facebookPageId: String, id: ID!, ignoreScraper: Boolean, isReviewPending: Boolean, isSuspended: Boolean, latitude: Float, longitude: Float, name: String, twitter: String, url: URL): Venue
    deleteGenre(id: ID!): Boolean
    deleteGenreCategory(id: ID!): Boolean
    deleteRegion(id: ID!): Boolean
    deleteUser(id: ID!): Boolean
    deleteVenue(id: ID!): Boolean
    hideEvent(hide: Boolean!, id: ID!): Event
    promoteEvent(id: ID!, promote: Boolean!): Event
    reviewEvent(description: String, from: Date, genres: [ID], hasTime: Boolean, id: ID!, isHidden: Boolean, isPromoted: Boolean, links: [EventLinkInput], until: Date, venueId: ID): Event
    suggestEvent(description: String!, from: Date!, hasTime: Boolean, id: ID, until: Date, venueId: ID!): Event
}

type Query {
    authenticate(email: String!, password: String!): Authentication
    event(id: ID!): Event
    findSimilarEvents(from: String!, venueId: String!): [Event]
    genre(id: ID!): Genre
    genreCategory(id: ID): GenreCategory
    genres(ids: [ID]!): [Genre]!
    region(id: ID, slug: String): Region
    regions: [Region]
    regionsList(listOptions: ListOptionsInput): RegionsList!
    venue(id: ID!): Venue
}

type Region {
    createdAt: Date!
    dayOffset: Float!
    email: String!
    events(eventDays: [EventDay!]!, withHidden: Boolean): [[Event]]
    eventsList(listOptions: ListOptionsInput, search: String): EventsList!
    facebookPage: String
    footerUrl: URL
    genreCategoriesList(listOptions: ListOptionsInput): GenreCategoriesList!
    genresList(listOptions: ListOptionsInput, search: String): GenresList!
    id: ID!
    latitude: Float!
    longitude: Float!
    name: String!
    slug: String!
    suspendedUntil: Date
    timeZone: String!
    twitterAccount: String
    updatedAt: Date!
    venues: [Venue]!
    venuesList(listOptions: ListOptionsInput, search: String): VenuesList!
}

type RegionsList {
    count: Int!
    regions: [Region]!
}

type User {
    email: String!
    id: ID!
    isSuperuser: Boolean!
    name: String!
    region: Region
}

type Venue {
    address: String
    aliases: [VenueAlias]
    createdAt: Date!
    email: String
    events(eventDays: [EventDay!]!, withHidden: Boolean): [[Event]]
    facebookPageId: String
    id: ID!
    ignoreScraper: Boolean!
    isReviewPending: Boolean!
    isSuspended: Boolean!
    latitude: Float
    longitude: Float
    name: String!
    region: Region!
    regionId: ID!
    twitter: String
    updatedAt: Date!
    url: String
}

type VenueAlias {
    id: ID!
    name: String!
}

type VenuesList {
    count: Int!
    venues: [Venue]!
}

enum CacheControlScope {
    PRIVATE
    PUBLIC
}

input EventLinkInput {
    label: String!
    url: URL!
}

input ListOptionsInput {
    pagination: PaginationInput
    sort: SortInput
}

input PaginationInput {
    page: Int!
    perPage: Int!
}

input ScraperEventInput {
    description: String!
    from: Date!
    genres: [String]
    hasTime: Boolean
    links: [EventLinkInput]
    regionId: ID!
    sourceIdentifier: String!
    sourcePriority: Int
    sourceUrl: URL!
    until: Date
    venueName: String!
}

input SortInput {
    ascending: Boolean
    field: String!
}


"ISO 8601 date string"
scalar Date

"URL string"
scalar URL

"Date with format: YYYY-MM-DD"
scalar EventDay

"password string with at least 8 characters"
scalar password

"The `Upload` scalar type represents a file upload."
scalar Upload
