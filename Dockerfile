FROM docker.io/library/node:16-alpine

RUN apk add --no-cache git

WORKDIR /opt/denkmal-scraper
COPY package.json .
COPY yarn.lock .
RUN yarn install

COPY . .

CMD ["yarn", "run", "scraper"]
