module.exports = {
    preset: 'ts-jest',
    testEnvironment: 'node',
    modulePaths: ["<rootDir>/src/"],
    setupFilesAfterEnv: ["<rootDir>/src/test/setupTestFramework.ts"],
};
